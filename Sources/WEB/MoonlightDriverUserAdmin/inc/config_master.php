<?php
	/**
	 * URL
	 */
	define('CONF_URL_INDEX',			CONF_URL_ROOT . "index.php");
	define('CONF_URL_ERROR',			CONF_URL_ROOT . "error.php");
	define('CONF_URL_AJAX',				CONF_URL_ROOT . "ajax/ajax.php");
	
	define('CONF_URL_MENU',				CONF_PATH_ROOT . "menu.php");
	define('CONF_URL_FOOTER',			CONF_PATH_ROOT . "footer.php");
	
	define('CONF_URL_LOGIN_ROOT',		CONF_URL_ROOT . "login/");
	define('CONF_URL_LOGIN',			CONF_URL_LOGIN_ROOT . "login.php");
	define('CONF_URL_LOGOUT',			CONF_URL_LOGIN_ROOT . "logout.php");
	define('CONF_URL_LOGIN_ACTION',		CONF_URL_LOGIN_ROOT . "login_action.php");
	
	define('CONF_URL_PAGE_ROOT',			CONF_URL_ROOT . "pages/");
	define('CONF_URL_USER_MANAGEMENT',		CONF_URL_ROOT . "pages/user_management.php");
	define('CONF_URL_CALL_MANAGEMENT',		CONF_URL_ROOT . "pages/call_management.php");
	define('CONF_URL_CRACKDOWN_MANAGEMENT',	CONF_URL_ROOT . "pages/crackdown_management.php");
	define('CONF_URL_COMPANY_MANAGEMENT',	CONF_URL_ROOT . "pages/company_management.php");
	define('CONF_URL_ACCOUNT_MANAGEMENT',	CONF_URL_ROOT . "pages/account_management.php");
	define('CONF_URL_NOTICE_MANAGEMENT',	CONF_URL_ROOT . "pages/notice_management.php");
	define('CONF_URL_CRACKDOWN_RANK_MANAGEMENT',	CONF_URL_ROOT . "pages/crackdown_rank_management.php");
	define('CONF_URL_CRACKDOWN_BLACKLIST_MANAGEMENT',	CONF_URL_ROOT . "pages/crackdown_blacklist_management.php");
	
	define('CONF_URL_FILE_NOTICE',			CONF_URL_FILE_ROOT . "notice/");
	define('CONF_PATH_FILE_NOTICE',			CONF_PATH_FILE_ROOT . "notice/");
	define('CONF_URL_FILE_PUSH',			CONF_URL_FILE_ROOT . "push/");
	define('CONF_PATH_FILE_PUSH',			CONF_PATH_FILE_ROOT . "push/");
	
	define("GOOGLE_API_KEY", "AIzaSyDYQVSDIL8OgaS_n7X3QRgzo1f8I0ahVm0");
	define("GOOGLE_SERVER_KEY", "AIzaSyDb5DTLNl-uMS820Bozj5S1AVohg1g_uHY");
	define("TMAP_KEY", "013703a7-d144-36c8-a049-9dafe4e4417b");
	
	define('MAX_PUS_MESSAGE_LENGTH',	4000);
	
	define("CONF_SITE_TITLE", "카대리 :: 관리자 페이지");
	
	$ROLE = array(
		1 => "admin",
		2 => "manager",
		3 => "employee"
	);
	
	$ROLE_LEVEL = array(
		"account" => array("inquiry" => 1, "create" => 1, "modify" => 1, "delete" => 1),
		"user" => array("inquiry" => 2, "create" => 1, "modify" => 1, "delete" => 1), 
		"call" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"crackdown" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"company" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"notice" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"crackdown_rank" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"crackdown_blacklist" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3)
	);
	
	$REGION = array("전국콜", "서울경기", "강원도", "충청도", "경상도", "전라도", "제주도", "대구");
	
	$CALL_SATATUS = array(
		"wait" => "배차 대기",
		"catch" => "배차 완료",
		"start" => "운행 시작",
		"end" => "운행 종료"
	);
?>