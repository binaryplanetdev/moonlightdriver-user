<?php
	session_start();
	
	date_default_timezone_set('Asia/Seoul');
	
	/**
	 * DATABASE
	 */
	define("DEF_MONGO_IP",		"dev.sncinteractive.com");
	define("DEF_MONGO_PORT",	"");
	define("DEF_MONGO_USER",	"");
	define("DEF_MONGO_PASS",	"");
	define("DEF_MONGO_DB",		"dev_moonlight");
	
	/**
	 * PATH
	 */
	define('CONF_URL_ROOT',			"/");
	define('CONF_PATH_ASSETS', 		CONF_URL_ROOT . "assets/");
	
	define("CONF_PATH_ROOT",		$_SERVER["DOCUMENT_ROOT"] . "/");
	define("CONF_PATH_CLASS",		CONF_PATH_ROOT . "inc/classes/");
	define('CONF_PATH_DEBUG_FILE',	CONF_PATH_ROOT . "logs/");
	
	define('CONF_URL_FILE_ROOT',	CONF_URL_ROOT . "upload/");
	define('CONF_PATH_FILE_ROOT',	CONF_PATH_ROOT . "upload/");
	
	/**
	 * DEBUG
	 */
	define('CONF_DEBUG',			true);
	define('CONF_DEBUG_INFO',		true);
	define('CONF_DEBUG_ERROR',		true);
	define('CONF_DEBUG_DISPLAY',	false);
	define('CONF_DEBUG_SAVE',		true);
	define('CONF_DEBUG_SAVE_FILE_ERROR',	'snc_debug_mesg_error.log');
	define('CONF_DEBUG_SAVE_FILE_ETC',		'snc_debug_mesg_etc.log');
	
	define('PRODUCTION_CODE',		'DEV');
	
	/**
	 * DEFINE
	 */
	include_once('config_master.php');
	
	/**
	 * GLOBAL FUNCTIONS
	 */
	include_once("global_function.php");
?>