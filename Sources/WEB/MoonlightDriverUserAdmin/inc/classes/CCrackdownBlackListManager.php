<?php 
	class CCrackdownBlackListManager {
		var $crackdownblacklists;
		
		function CCrackdownBlackListManager($_crackdownblacklists) {
			$this->crackdownblacklists = $_crackdownblacklists;
		}
		
		function getTotalCrackdownBlackListCount() {
			return $this->crackdownblacklists->count();
		}
		
		function getTotalFilteredCrackdownBlackListCount($_find) {
			return $this->crackdownblacklists->count($_find);
		}
		
		function getCrackdownBlackListList($_find, $_start, $_length, $_sort) {
			$crackdown_blacklist_list = $this->crackdownblacklists->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			$ret_crackdown_blacklist_list = array();
			foreach ($crackdown_blacklist_list as $row) {
				$rowArray = array();
				
				$rowArray['userId'] = $row['userId'];
				$rowArray['phone'] = $row['phone'];
				$rowArray['wrongReportCount'] = $row['wrongReportCount'];
				$rowArray['lastWrongCrackdownReportTime'] = date("Y-m-d H:i:s", ($row["lastWrongCrackdownReportTime"] / 1000));
				$rowArray['blockState'] = $row['blockState'];
				$rowArray['blackListUpdateTime'] = date("Y-m-d H:i:s", ($row["blackListUpdateTime"] / 1000));
		
				$ret_crackdown_blacklist_list[] = $rowArray;
			}
			
			return $ret_crackdown_blacklist_list;
		}
		
		function getCrackdownBlackListDetail($_userId) {
			$ret_crackdown_blacklist_info = array();
			$crackdown_blacklist_info = $this->crackdownblacklists->findOne(array('userId' => $_userId));
			
			if(isset($crackdown_blacklist_info)) {
				$ret_crackdown_blacklist_info['userId'] = $crackdown_blacklist_info['userId'];
				$ret_crackdown_blacklist_info['phone'] = $crackdown_blacklist_info['phone'];
				$ret_crackdown_blacklist_info['blockState'] = $crackdown_blacklist_info['blockState'];
				$ret_crackdown_blacklist_info['wrongReportCount'] = $crackdown_blacklist_info['wrongReportCount'];
				$ret_crackdown_blacklist_info['lastWrongCrackdownReportTime'] = $crackdown_blacklist_info['lastWrongCrackdownReportTime'];
				$ret_crackdown_blacklist_info['blackListUpdateTime'] = $crackdown_blacklist_info['blackListUpdateTime'];
			}
			
			return $ret_crackdown_blacklist_info;
		}

		function addCrackdownBlackList($_userId, $_phone, $_blockState, $_wrongReportCount, $_lastWrongCrackdownReportTime) {
			$newData = array(
				'userId' => $_userId,
				'phone' => $_phone,
				'blockState' => $_blockState,
				'wrongReportCount' => $_wrongReportCount,
				'lastWrongCrackdownReportTime' => $_lastWrongCrackdownReportTime,
				'blackListUpdateTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->crackdownblacklists->insert($newData);
				
			return $ret;
		}
		
		function updateCrackdownBlackList($_userId, $_wrongReportCount, $_lastWrongCrackdownReportTime) {
			$updateData = array(
				'wrongReportCount' => $_wrongReportCount,
				'lastWrongCrackdownReportTime' => $_lastWrongCrackdownReportTime,
				'blackListUpdateTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->crackdownblacklists->update(array('userId' => $_userId), array('$set' => $updateData));
				
			return $ret;
		}
		
		function updateCrackdownBlockState($_userId, $_blockState) {
			$updateData = array(
				'blockState' => $_blockState,
				'blackListUpdateTime' => floatval(round(microtime(true) * 1000))
			);
				
			$ret = $this->crackdownblacklists->update(array('userId' => $_userId), array('$set' => $updateData));
		
			return $ret;
		}
		
		function deleteCrackdownBlackList($_userId) {
			$this->crackdownblacklists->remove(array('userId' => $_userId));
		}
	}
?>