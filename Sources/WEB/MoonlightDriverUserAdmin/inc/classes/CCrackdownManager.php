<?php 
	class CCrackdownManager {
		var $crackdowns;
		var $CrackdownBlackListManager;
		var $UserManager;
		
		function CCrackdownManager($_crackdowns) {
			$this->crackdowns = $_crackdowns;
		}
		
		function getUserManagerClass($_users) {
			if ($this->UserManager) {
				return;
			}
		
			require_once_classes(Array('CUserManager'));
			$this->UserManager = new CUserManager($_users);
		}
		
		function getCrackdownBlackListManagerClass($_crackdownblacklists) {
			if ($this->CrackdownBlackListManager) {
				return;
			}
		
			require_once_classes(Array('CCrackdownBlackListManager'));
			$this->CrackdownBlackListManager = new CCrackdownBlackListManager($_crackdownblacklists);
		}
		
		function getTotalCrackdownCount() {
			return $this->crackdowns->count();
		}
		
		function getTotalFilteredCrackdownCount($_find) {
			return $this->crackdowns->count($_find);
		}
		
		function getCrackdownList($_find, $_start, $_length, $_sort) {
			$crackdown_list = $this->crackdowns->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			$ret_crackdown_list = array();
			foreach ($crackdown_list as $row) {
				$rowArray = array();
				
				$rowArray['crackdown_id'] = strval($row['_id']);
				$rowArray['createTime'] = date("Y-m-d H:i:s", ($row["createTime"] / 1000));
				$rowArray['from'] = $row['from'];
				$rowArray['reportType'] = $row['reportType'] == "normal" ? "일반" : ($row['reportType'] == "voice" ? "음성" : "이미지");
				$rowArray['address'] = $row['sido'] . " " . $row['sigugun'] . " " . $row['address'];
				$rowArray['phone'] = $row['phone'];				
				$rowArray['mapLocation'] = "<button type='button' class='btn btn-info btn-sm btn_map_location' data-crackdownid='" . $rowArray['crackdown_id'] . "'>보기</button>";
				$rowArray['updatedTime'] = date("Y-m-d H:i:s", ($row["updatedTime"] / 1000));
				$rowArray['deletion'] = "<button type='button' class='btn btn-danger btn-sm btn_delete_crackdown'>삭제</button>";
		
				$ret_crackdown_list[] = $rowArray;
			}
			
			return $ret_crackdown_list;
		}
		
		function getCrackdownInfo($_crackdown_id) {
			$ret_crackdown_info = array();
			$crackdown_info = $this->crackdowns->findOne(array('_id' => new MongoId($_crackdown_id)));
				
			if(isset($crackdown_info)) {
				$ret_crackdown_info['crackdown_id'] = strval($crackdown_info['_id']);
				$ret_crackdown_info['userId'] = $crackdown_info['userId'];
				$ret_crackdown_info['registrant'] = $crackdown_info['registrant'];
				$ret_crackdown_info['phone'] = $crackdown_info['phone'];
				$ret_crackdown_info['reportType'] = $crackdown_info['reportType'];
				$ret_crackdown_info['picture'] = $crackdown_info['picture'];
				$ret_crackdown_info['dislikeCount'] = $crackdown_info['dislikeCount'];
				$ret_crackdown_info['likeCount'] = $crackdown_info['likeCount'];
				$ret_crackdown_info['commentCount'] = $crackdown_info['commentCount'];
				$ret_crackdown_info['from'] = $crackdown_info['from'];
				$ret_crackdown_info['sido'] = $crackdown_info['sido'];
				$ret_crackdown_info['sigugun'] = $crackdown_info['sigugun'];
				$ret_crackdown_info['locations'] = $crackdown_info['locations'];
				$ret_crackdown_info['address'] = $crackdown_info['address'];
				$ret_crackdown_info['createTime'] = date("Y-m-d H:i:s", ($crackdown_info["createTime"] / 1000));
				$ret_crackdown_info['updatedTime'] = date("Y-m-d H:i:s", ($crackdown_info["updatedTime"] / 1000));
			}
				
			return $ret_crackdown_info;
		}

		function addCrackdown($_users, $_reportType, $_phone, $_sido, $_gugun, $_address, $_lat, $_lng) {
			$this->getUserManagerClass($_users);
			
			$userInfo = $this->UserManager->getUserInfoByPhoneNumber($_phone);
			$newData = array(
				'userId' => isset($userInfo) ? $userInfo['user_id'] : "",
				'registrant' => generateScretRegistrant($_phone),
				'phone' => $_phone,
				'reportType' => $_reportType,
				'picture' => "",
				'dislikeCount' => 0,
				'likeCount' => 0,
				'commentCount' => 0,
				'from' => "카대리",
				'sido' => $_sido,
				'sigugun' => $_gugun,
				'locations' => array(
					"type" => "Point",
					"coordinates" => array(floatval($_lng), floatval($_lat))
				),
				'address' => $_address,
				'createTime' => floatval(round(microtime(true) * 1000)),
				'updatedTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->crackdowns->insert($newData);
				
			return $ret;
		}
		
		function updateCrackdown($_users, $_crackdown_id, $_reportType, $_phone, $_sido, $_gugun, $_address, $_lat, $_lng) {
			$this->getUserManagerClass($_users);
				
			$userInfo = $this->UserManager->getUserInfoByPhoneNumber($_phone);
			$updateData = array(
				'userId' => isset($userInfo) ? $userInfo['user_id'] : "",
				'registrant' => generateScretRegistrant($_phone),
				'phone' => $_phone,
				'reportType' => $_reportType,
				'picture' => "",
				'dislikeCount' => 0,
				'likeCount' => 0,
				'commentCount' => 0,
				'from' => "카대리",
				'sido' => $_sido,
				'sigugun' => $_gugun,
				'locations' => array(
					"type" => "Point",
					"coordinates" => array(floatval($_lng), floatval($_lat))
				),
				'address' => $_address,
				'updatedTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->crackdowns->update(array('_id' => new MongoId($_crackdown_id)), array('$set' => $updateData));
				
			return $ret;
		}
		
		function deleteCrackdown($_crackdown_id) {
			$this->crackdowns->remove(array('_id' => new MongoId($_crackdown_id)));
		}
		
		function deleteSelectedCrackdown($_crackdownblacklists, $_crackdown_id_list, $_wrong_crackdown_id_list) {
			if(isset($_wrong_crackdown_id_list) && !empty($_wrong_crackdown_id_list)) {
				$this->getCrackdownBlackListManagerClass($_crackdownblacklists);
				
				foreach ($_wrong_crackdown_id_list as $_crackdown_id) {
					$crackdown_info = $this->crackdowns->findOne(array('_id' => new MongoId($_crackdown_id), 'from' => "카대리"));
					
					if(isset($crackdown_info) && !empty($crackdown_info)) {
						$crackdown_blacklist_info = $this->CrackdownBlackListManager->getCrackdownBlackListDetail($crackdown_info['userId']);
						
						if(isset($crackdown_blacklist_info) && !empty($crackdown_blacklist_info)) {
							$lastWrongCrackdownReportTime = $crackdown_blacklist_info['lastWrongCrackdownReportTime'] > $crackdown_info["createTime"] ? $crackdown_blacklist_info['lastWrongCrackdownReportTime'] : $crackdown_info["createTime"];
							
							$this->CrackdownBlackListManager->updateCrackdownBlackList($crackdown_info['userId'], $crackdown_blacklist_info['wrongReportCount'] + 1, $lastWrongCrackdownReportTime);							
						} else {
							$this->CrackdownBlackListManager->addCrackdownBlackList($crackdown_info['userId'], $crackdown_info['phone'], 'nonblock', 1, $crackdown_info["createTime"]);
						}
					}
				}
			}
			
			if(isset($_crackdown_id_list) && !empty($_crackdown_id_list)) {
				foreach ($_crackdown_id_list as $_crackdown_id) {
					$this->crackdowns->remove(array('_id' => new MongoId($_crackdown_id)));
				}
			}
		}
	}
?>