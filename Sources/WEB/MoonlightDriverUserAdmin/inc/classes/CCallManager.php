<?php 
	class CCallManager {
		var $calls;
		var $UserManager;
		
		function CCallManager($_calls) {
			$this->calls = $_calls;
		}
		
		function getUserManagerClass($_users) {
			if ($this->UserManager) {
				return;
			}
		
			require_once_classes(Array('CUserManager'));
			$this->UserManager = new CUserManager($_users);
		}
		
		function getTotalCallCount() {
			return $this->calls->count();
		}
		
		function getTotalFilteredCallCount($_find) {
			return $this->calls->count($_find);
		}
		
		function getCallList($_find, $_start, $_length, $_sort) {
			$call_list = $this->calls->find($_find)->sort($_sort)->skip($_start)->limit($_length);
				
			global $CALL_SATATUS;
			
			$ret_call_list = array();
			foreach ($call_list as $row) {
				$rowArray = array();
				
				$rowArray['call_id'] = strval($row['_id']);
				$rowArray['callSeq'] = $row['callSeq'];
				$rowArray['createdTime'] = date("Y-m-d H:i:s", ($row["createdTime"] / 1000));
				$rowArray['userPhone'] = $row['user']["phone"];
				$rowArray['currentPosition'] = $row['user']['address'];
				$rowArray['startPosition'] = ((isset($row['start']['poi_name']) && !empty($row['start']['poi_name'])) ? ($row['start']['poi_name'] . " : ") : "") . $row['start']['address'];
				$rowArray['endPosition'] = ((isset($row['end']['poi_name']) && !empty($row['end']['poi_name'])) ? ($row['end']['poi_name'] . " : ") : "") . $row['end']['address'];
				$rowArray['throughPosition'] = ((isset($row['through']['poi_name']) && !empty($row['through']['poi_name'])) ? ($row['through']['poi_name'] . " : ") : "") . $row['through']['address'];
				$rowArray['status'] = $CALL_SATATUS[$row['status']];
				$rowArray['money'] = $row['money'];
				$rowArray['etc'] = "<div class='line-ellipsis'>" . $row['etc'] . "</div>";
				$rowArray['mapLocation'] = "<button type='button' class='btn btn-info btn-sm btn_map_location' data-callid='" . $rowArray['call_id'] . "'>보기</button>";
				$rowArray['deletion'] = "<button type='button' class='btn btn-danger btn-sm btn_delete_call'>삭제</button>";
		
				$ret_call_list[] = $rowArray;
			}
			
			return $ret_call_list;
		}
		
		function getCallInfo($_call_id) {
			$ret_call_info = array();
			$call_info = $this->calls->findOne(array('_id' => new MongoId($_call_id)));
				
			if(isset($call_info)) {
				$ret_call_info['call_id'] = strval($call_info['_id']);
				$ret_call_info['callSeq'] = $call_info['callSeq'];
				$ret_call_info['callType'] = $call_info['callType'];
				$ret_call_info['money'] = $call_info['money'];
				$ret_call_info['payment_option'] = $call_info['payment_option'];
				$ret_call_info['etc'] = $call_info['etc'];
				$ret_call_info['status'] = $call_info['status'];
				$ret_call_info['driver'] = $call_info['driver'];
				$ret_call_info['user'] = $call_info['user'];
				$ret_call_info['end'] = $call_info['end'];
				$ret_call_info['through'] = $call_info['through'];
				$ret_call_info['start'] = $call_info['start'];
				$ret_call_info['createdTime'] = date("Y-m-d H:i:s", ($call_info["createdTime"] / 1000));
				$ret_call_info['updatedTime'] = date("Y-m-d H:i:s", ($call_info["updatedTime"] / 1000));
			}
				
			return $ret_call_info;
		}

		function addCall($_callSeq, $_callType, $_money, $_payment_option, $_etc, $_status, $_driver, $_user, $_start, $_end, $_through) {
			$newData = array(
				'callSeq' => $_callSeq,
				'callType' => $_callType,
				'money' => $_money,
				'payment_option' => $_payment_option,
				'etc' => $_etc,
				'status' => $_status,
				'driver' => $_driver,
				'user' => $_user,
				'start' => $_start,
				'end' => $_end,
				'through' => $_through,
				'createdTime' => floatval(round(microtime(true) * 1000)),
				'updatedTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->calls->insert($newData);
				
			return $ret;
		}
		
		function updateCall($_call_id, $_callType, $_money, $_payment_option, $_etc, $_status, $_user, $_start, $_end, $_through) {
			$updateData = array(
				'callType' => $_callType,
				'money' => $_money,
				'payment_option' => $_payment_option,
				'etc' => $_etc,
				'status' => $_status,
				'user' => $_user,
				'start' => $_start,
				'end' => $_end,
				'through' => $_through,
				'updatedTime' => floatval(round(microtime(true) * 1000))
			);
			
			$ret = $this->calls->update(array('_id' => new MongoId($_call_id)), array('$set' => $updateData));
				
			return $ret;
		}
		
		function updateCallStatus($_call_id, $_status) {
			$updateData = array(
				'status' => $_status,
				'updatedTime' => floatval(round(microtime(true) * 1000))
			);
				
			$ret = $this->calls->update(array('_id' => new MongoId($_call_id)), array('$set' => $updateData));
		
			return $ret;
		}
		
		function deleteCall($_call_id) {
			$this->calls->remove(array('_id' => new MongoId($_call_id)));
		}
	}
?>