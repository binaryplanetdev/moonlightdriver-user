var list_table;

$(document).ready(function() {
	list_table = $('#account_list').DataTable({
		initComplete: function () {
			
		},
		drawCallback: function() {
			$('#account_list > tbody').off("click").on('click', 'tr', function (event) {
				var index = $(this).index("#account_list > tbody > tr");
				
				if($(event.target).is('#account_list > tbody > tr:eq(' + index + ') > td:eq(0),#account_list > tbody > tr:eq(' + index + ') > td:eq(1),#account_list > tbody > tr:eq(' + index + ') > td:eq(2),#account_list > tbody > tr:eq(' + index + ') > td:eq(3),#account_list > tbody > tr:eq(' + index + ') > td:eq(4),#account_list > tbody > tr:eq(' + index + ') > td:eq(5)')) {
					var row_data = list_table.row(this).data();
					
					if(row_data) {
						getAccountDetail(row_data.account_id);
					}
				}
			});

			$(".btn_delete_account").off("click").on("click", function() {
				var ret = confirm("삭제하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						deleteAccount(row_data.account_id);
					}
				}
			});
		},
		columns: [
		    { data: "account_id", visible: false, orderable: false, searchable: false },
		    { data: "id" },
			{ data: "name" },
			{ data: "phone" },
			{ data: "email" },
			{ data: "role" },
			{ data: "createdDate" },
			{ data: "deletion", orderable: false, searchable: false, class: "text-center" }
		],
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxAccount.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getAccountList";
			},
			error: function() {  // error handling code
			}
		}
	});
	
	$('#btnCreateAccount').off("click").on('click', function() {
		showAccountDetailPopup(null);
	});
});

function getAccountDetail(_account_id) {
	var params = {};
	params.type = "account_detail";
	params.account_id = _account_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxAccount.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showAccountDetailPopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showAccountDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("계정 상세보기");
	} else {
		$("#modal_popup_label").text("계정 등록");
	}
	
	$("#modal_popup_content").html(getAccountContentsHtml(mode));
	
	var footerHtml = "";
	
	initRoleLevelData();
	
	if(mode == "edit") {
		$("#id").val(_data.id);
		$("#name").val(_data.name);
		$("#phone").val(_data.phone);
		$("#email").val(_data.email);
		$("#role_level").val(_data.level);
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			editAccount(_data);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			addAccount();
		});
	}
}

function initRoleLevelData() {
	var html = "";
	
	html += "<option value='0'>권한 선택</option>";
	$.each(role_level, function(_key, _val) {
		html += "<option value='" + _key + "'>" + _val + "</option>";
	});
	
	$("#role_level").html(html);
}

function getAccountContentsHtml(_mode) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='id'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>아이디</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='id'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='pw'>";

	if(_mode == "create") {
		html += "				<span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>";
	}
	
	html += "				비밀번호";
	html += "			</label>";
	html += "			<div class='col-sm-8'><input type='password' class='form-control' id='pw'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='name'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>이름</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='name'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='phone'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>휴대폰</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='phone'></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='email'>이메일</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='email'></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='role_level'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>권한</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='role_level'></select>";
	html += "			</div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function editAccount(_account_info) {
	var params = {};
	params.type = "edit_account";
	params.account_id = _account_info.account_id;
	params.id = $.trim($("#id").val());
	params.pw = $.trim($("#pw").val());
	params.name = $.trim($("#name").val());
	params.phone = $.trim($("#phone").val());
	params.email = $.trim($("#email").val());
	params.level = $.trim($("#role_level").val());
	
	if(params.id.length <= 0) {
		alert("아이디를 입력해주세요.");
		$("#id").focus();
		return;
	}
	
	if(params.pw.length <= 0) {
		alert("비밀번호를 입력해주세요.");
		$("#pw").focus();
		return;
	}
	
	if(params.name.length <= 0) {
		alert("이름을 입력해주세요.");
		$("#name").focus();
		return;
	}
	
	if(params.phone.length <= 0) {
		alert("휴대폰 번호를 입력해주세요.");
		$("#phone").focus();
		return;
	}
	
	if(params.level == 0 || params.level.length <= 0) {
		alert("권한을 입력해주세요.");
		$("#role_level").focus();
		return;
	}
	
	$(".loading").show();
	
	$.post("/ajax/ajaxAccount.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function addAccount() {
	var params = {};
	params.type = "add_account";
	params.id = $.trim($("#id").val());
	params.pw = $.trim($("#pw").val());
	params.name = $.trim($("#name").val());
	params.phone = $.trim($("#phone").val());
	params.email = $.trim($("#email").val());
	params.level = $.trim($("#role_level").val());
	params.ynPush = $("input:checkbox[id='ynPush']").is(":checked") ? "Y" : "N";
	
	if(params.id.length <= 0) {
		alert("아이디를 입력해주세요.");
		$("#id").focus();
		return;
	}
	
	if(params.pw.length <= 0) {
		alert("비밀번호를 입력해주세요.");
		$("#pw").focus();
		return;
	}
	
	if(params.name.length <= 0) {
		alert("이름을 입력해주세요.");
		$("#name").focus();
		return;
	}
	
	if(params.phone.length <= 0) {
		alert("휴대폰 번호를 입력해주세요.");
		$("#phone").focus();
		return;
	}
	
	if(params.level == 0 || params.level.length <= 0) {
		alert("권한을 입력해주세요.");
		$("#role_level").focus();
		return;
	}
	
	$(".loading").show();
	
	$.post("/ajax/ajaxAccount.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function deleteAccount(_account_id) {
	var params = {};
	params.type = "delete_account";
	params.account_id = _account_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxAccount.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}