var map;
var center = { lat : 37.566221, lng : 126.977921 };

$(document).ready(function() {

});

function initMap() {
	var bounds = new google.maps.LatLngBounds(null);
	
	center = {
		lat : callDetail.start.location.coordinates[1],
		lng : callDetail.start.location.coordinates[0]
	};
	
	map = new google.maps.Map(document.getElementById('map'), {
		mapTypeControl: false,
		center: center,
		zoom: 17
	});
	
	if(callDetail.start && callDetail.start.address) {
		var startPosition = {
			lat : callDetail.start.location.coordinates[1],
			lng : callDetail.start.location.coordinates[0]
		};
		
		var marker = new google.maps.Marker({
			position: startPosition,
			map: map
		});
		
		var infowindow = new google.maps.InfoWindow({
			content: "출발지 : " + callDetail.start.address
		});
		
		marker.addListener('click', function() {
			if(infowindow) {
				infowindow.open(map, marker);
			}
		});

		if(infowindow) {
			infowindow.open(map, marker);
		}
		
		bounds.extend(startPosition);
	}
	
	if(callDetail.end && callDetail.end.address) {
		var endPosition = {
			lat : callDetail.end.location.coordinates[1],
			lng : callDetail.end.location.coordinates[0]
		};
		
		var marker = new google.maps.Marker({
			position: endPosition,
			map: map
		});
		
		var infowindow = new google.maps.InfoWindow({
			content: "도착지 : " + callDetail.end.address
		});
		
		marker.addListener('click', function() {
			if(infowindow) {
				infowindow.open(map, marker);
			}
		});

		if(infowindow) {
			infowindow.open(map, marker);
		}
		
		bounds.extend(endPosition);
	}
	
	if(callDetail.through && callDetail.through.address) {
		var throughPosition = {
			lat : callDetail.through.location.coordinates[1],
			lng : callDetail.through.location.coordinates[0]
		};
		
		var marker = new google.maps.Marker({
			position: throughPosition,
			map: map
		});
		
		var infowindow = new google.maps.InfoWindow({
			content: "경유지 : " + callDetail.through.address
		});
		
		marker.addListener('click', function() {
			if(infowindow) {
				infowindow.open(map, marker);
			}
		});

		if(infowindow) {
			infowindow.open(map, marker);
		}
		
		bounds.extend(throughPosition);
	}
	
	google.maps.event.addListener(map, 'idle', function() {
		console.log("zoom : " + map.getZoom());
	});
	
	map.fitBounds(bounds);
}
