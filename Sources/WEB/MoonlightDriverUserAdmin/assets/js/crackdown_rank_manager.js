var list_table;
var rank_type = "";

$(document).ready(function() {
	rank_type = $(".nav.nav-tabs > li.active > a").attr("href");
	
	list_table = $('#crackdown_rank_list').DataTable({
		initComplete: function () {

		},
		drawCallback: function() {
			$('#crackdown_rank_list > tbody').off("click").on('click', 'tr', function (event) {
				var index = $(this).index("#crackdown_rank_list > tbody > tr");
				
				if($(event.target).is('#crackdown_rank_list > tbody > tr:eq(' + index + ') > td:eq(0),#crackdown_rank_list > tbody > tr:eq(' + index + ') > td:eq(1),#crackdown_rank_list > tbody > tr:eq(' + index + ') > td:eq(2),#crackdown_rank_list > tbody > tr:eq(' + index + ') > td:eq(3),#crackdown_rank_list > tbody > tr:eq(' + index + ') > td:eq(4)')) {
					var row_data = list_table.row(this).data();
					
					if(row_data) {
						getCrackdownRankDetail(row_data.crackdown_rank_id);
					}
				}
			});

			$(".btn_delete_crackdown_rank").off("click").on("click", function() {
				var ret = confirm("삭제하시겠습니까?");
				if(ret) {
					var row_data = list_table.row($(this).parent("td").parent("tr")).data();
					
					if(row_data) {
						deleteCrackdownRank(row_data.crackdown_rank_id);
					}
				}
			});
		},
		columns: [
		    { data: "crackdown_rank_id", visible: false, orderable: false, searchable: false },
		    { data: "month" },
			{ data: "weekOfYear" },
			{ data: "phone" },
			{ data: "normalCount" },
			{ data: "voiceCount" },
			{ data: "photoCount" },
			{ data: "totalCount" },
			{ data: "point" },
			{ data: "deletion", orderable: false, searchable: false, class: "text-center" }
		],
        serverSide: true,
		ajax: {
			url :"/ajax/ajaxCrackdownRank.php", // json datasource
			type: "post",  // type of method  , by default would be get
			data: function (d) {
				d.type = "getCrackdownRankList";
				d.rank_type = rank_type;
			},
			error: function() {  // error handling code
			}
		}
	});
	
	$('#btnCreateCrackdownRank').off("click").on('click', function() {
		showCrackdownRankDetailPopup(null);
	});
	
	$(".nav.nav-tabs > li").off("click").on("click", "a", function(event) {
		rank_type = $(this).attr("href");
		list_table.columns.adjust().draw();
	});
});

function getCrackdownRankDetail(_crackdown_rank_id) {
	var params = {};
	params.type = "crackdown_rank_detail";
	params.rank_type = rank_type;
	params.crackdown_rank_id = _crackdown_rank_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdownRank.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			showCrackdownRankDetailPopup(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function showCrackdownRankDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("음주단속 제보 랭킹 상세보기");
	} else {
		$("#modal_popup_label").text("음주단속 제보 랭킹 등록");
	}
	
	$("#modal_popup_content").html(getCrackdownRankContentsHtml(mode));
	
	var footerHtml = "";
	
	if(mode == "edit") {
		$("#crackdown_rankName").val(_data.crackdown_rankName);
		$("#telNumber").val(_data.telNumber);
		$("#region").val(_data.region);
		$("#price").val(_data.price);
		$("#stopByFee").val(_data.stopByFee);
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			editCrackdownRank(_data);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			addCrackdownRank();
		});
	}
}

function getCrackdownRankContentsHtml(_mode) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='crackdown_rank_type'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>랭킹 구분</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<select class='form-control' id='crackdown_rank_type'>";
	html += "					<option value=''>구분 선택</option>";
	html += "					<option value='weekly'>주간랭킹</option>";
	html += "					<option value='monthly'>월간랭킹</option>";
	html += "				</select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='month'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>월</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='month'/>";
	html += "				<p class='help-block'>eg. 2017-01</p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='weekOfYear'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>주(시작일)</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='weekOfYear'/>";
	html += "				<p class='help-block'>eg. 53(2016-12-28)</p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='phone'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>제보자</label>";
	html += "			<div class='col-sm-8'>";
	html += "				<input type='text' class='form-control' id='phone'/>";
	html += "				<p class='help-block'>(-)제외, eg. 01012345678</p>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='normal_count'>일반제보</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='normal_count' value='0'></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='voice_count'>음성제보</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='voice_count' value='0'></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='photo_count'>사진제보</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='photo_count' value='0'></div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function editCrackdownRank(_crackdown_rank_info) {
	var params = {};
	params.type = "edit_crackdown_rank";
	params.rank_type = rank_type;
	params.crackdown_rank_id = _crackdown_rank_info.crackdown_rank_id;
	params.crackdown_rankName = $.trim($("#crackdown_rankName").val());
	params.telNumber = $.trim($("#telNumber").val());
	params.region = $.trim($("#region").val());
	params.price = $.trim($("#price").val());
	params.stopByFee = $.trim($("#stopByFee").val());
	
	if(params.crackdown_rankName.length <= 0) {
		alert("회사명을 입력해주세요.");
		$("#crackdown_rankName").focus();
		return;
	}
	
	if(params.telNumber.length <= 0) {
		alert("전화번호를 입력해주세요.");
		$("#telNumber").focus();
		return;
	}
	
	if(params.region == 0 || params.region.length <= 0) {
		alert("지역을 선택해주세요.");
		$("#region").focus();
		return;
	}
	
	if(params.price == 0 || params.price.length <= 0) {
		alert("가격을 입력해주세요.");
		$("#price").focus();
		return;
	}
	
	if(params.stopByFee.length <= 0) {
		alert("경유비를 입력해주세요.");
		$("#stopByFee").focus();
		return;
	}
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdownRank.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function addCrackdownRank() {
	var params = {};
	params.type = "add_crackdown_rank";
	params.rank_type = rank_type;
	params.crackdown_rankName = $.trim($("#crackdown_rankName").val());
	params.telNumber = $.trim($("#telNumber").val());
	params.region = $.trim($("#region").val());
	params.price = $.trim($("#price").val());
	params.stopByFee = $.trim($("#stopByFee").val());
	
	if(params.crackdown_rankName.length <= 0) {
		alert("회사명을 입력해주세요.");
		$("#crackdown_rankName").focus();
		return;
	}
	
	if(params.telNumber.length <= 0) {
		alert("전화번호를 입력해주세요.");
		$("#telNumber").focus();
		return;
	}
	
	if(params.region == 0 || params.region.length <= 0) {
		alert("지역을 선택해주세요.");
		$("#region").focus();
		return;
	}
	
	if(params.price == 0 || params.price.length <= 0) {
		alert("가격을 입력해주세요.");
		$("#price").focus();
		return;
	}
	
	if(params.stopByFee.length <= 0) {
		alert("경유비를 입력해주세요.");
		$("#stopByFee").focus();
		return;
	}
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdownRank.php", params, function(_ret_data) {
		$(".loading").hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function deleteCrackdownRank(_crackdown_rank_id) {
	var params = {};
	params.type = "delete_crackdown_rank";
	params.rank_type = rank_type;
	params.crackdown_rank_id = _crackdown_rank_id;
	
	$(".loading").show();
	
	$.post("/ajax/ajaxCrackdownRank.php", params, function(_ret_data) {
		$(".loading").hide();
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.columns.adjust().draw();
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}