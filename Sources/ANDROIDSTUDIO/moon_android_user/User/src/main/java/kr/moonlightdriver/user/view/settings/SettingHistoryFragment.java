package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.view.main.MainActivity;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingHistoryFragment extends Fragment implements View.OnClickListener{

    private ImageButton ib_setting_goback, ib_driver_rate_1, ib_driver_rate_2, ib_report_unfair_1, ib_report_unfair_2, btn_driver_rate_popup_close, btn_notify_popup_close;
    private View mView;
    private MainActivity mMainActivity;
    private SettingsFragment mFragment;
    private LinearLayout popup_apprisal_driver, popup_notify_irrational;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mView = inflater.inflate(R.layout.fragment_setting_history_manage, container, false);

            mMainActivity = (MainActivity)getActivity();

            popup_apprisal_driver = (LinearLayout) mView.findViewById(R.id.popup_apprisal_driver);
            popup_apprisal_driver.setVisibility(View.GONE);
            popup_notify_irrational = (LinearLayout) mView.findViewById(R.id.popup_notify_irrational);
            popup_notify_irrational.setVisibility(View.GONE);

            ib_setting_goback = (ImageButton) mView.findViewById(R.id.ib_setting_goback);
            ib_driver_rate_1 = (ImageButton) mView.findViewById(R.id.ib_driver_rate_1);
            ib_driver_rate_2 = (ImageButton) mView.findViewById(R.id.ib_driver_rate_2);
            ib_report_unfair_1 = (ImageButton) mView.findViewById(R.id.ib_report_unfair_1);
            ib_report_unfair_2 = (ImageButton) mView.findViewById(R.id.ib_report_unfair_2);
            btn_driver_rate_popup_close = (ImageButton) mView.findViewById(R.id.btn_driver_rate_popup_close);
            btn_notify_popup_close = (ImageButton)mView.findViewById(R.id.btn_notify_popup_close);

            ib_setting_goback.setOnClickListener(this);
            ib_driver_rate_1.setOnClickListener(this);
            ib_driver_rate_2.setOnClickListener(this);
            ib_report_unfair_1.setOnClickListener(this);
            ib_report_unfair_2.setOnClickListener(this);
            btn_driver_rate_popup_close.setOnClickListener(this);
            btn_notify_popup_close.setOnClickListener(this);

        }catch (Exception e){
            e.printStackTrace();
        }
        return mView;
    }

    @Override
    public void onClick(View _view) {
        try {
            if(_view == ib_setting_goback){
                getActivity().getSupportFragmentManager().beginTransaction().remove(SettingHistoryFragment.this).commit();
            }else if(_view == ib_driver_rate_1){
                popup_apprisal_driver.setVisibility(View.VISIBLE);
            }else if(_view == ib_driver_rate_2){
                popup_apprisal_driver.setVisibility(View.VISIBLE);
            }else if(_view == ib_report_unfair_1){
                popup_notify_irrational.setVisibility(View.VISIBLE);
            }else if(_view == ib_report_unfair_2){
                popup_notify_irrational.setVisibility(View.VISIBLE);
            }else if(_view == btn_driver_rate_popup_close){
                popup_apprisal_driver.setVisibility(View.GONE);
            }else if(_view == btn_notify_popup_close){
                popup_notify_irrational.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
