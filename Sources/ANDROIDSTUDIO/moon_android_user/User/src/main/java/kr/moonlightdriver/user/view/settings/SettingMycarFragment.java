package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.view.main.MainActivity;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingMycarFragment extends Fragment implements View.OnClickListener{

    private ImageButton ib_mycar_goback, ib_mycar_submit, ib_mycar_small, ib_mycar_middle, ib_mycar_large, ib_mycar_suv, ib_mycar_domestic, ib_mycar_foreign, ib_mycar_auto, ib_mycar_manual;
    private View mView;
    private MainActivity mMainActivity;
    private SettingsFragment mFragment;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mView = inflater.inflate(R.layout.fragment_setting_mycar, container, false);

            mMainActivity = (MainActivity)getActivity();

            ib_mycar_goback = (ImageButton) mView.findViewById(R.id.ib_mycar_goback);
            ib_mycar_submit = (ImageButton)mView.findViewById(R.id.ib_mycar_submit);
            ib_mycar_small = (ImageButton)mView.findViewById(R.id.ib_mycar_small);
            ib_mycar_middle = (ImageButton)mView.findViewById(R.id.ib_mycar_middle);
            ib_mycar_large = (ImageButton)mView.findViewById(R.id.ib_mycar_large);
            ib_mycar_suv = (ImageButton)mView.findViewById(R.id.ib_mycar_suv);
            ib_mycar_domestic = (ImageButton)mView.findViewById(R.id.ib_mycar_domestic);
            ib_mycar_foreign = (ImageButton)mView.findViewById(R.id.ib_mycar_foreign);
            ib_mycar_auto = (ImageButton)mView.findViewById(R.id.ib_mycar_auto);
            ib_mycar_manual = (ImageButton)mView.findViewById(R.id.ib_mycar_manual);

            ib_mycar_goback.setOnClickListener(this);
            ib_mycar_submit.setOnClickListener(this);
            ib_mycar_small.setOnClickListener(this);
            ib_mycar_middle.setOnClickListener(this);
            ib_mycar_large.setOnClickListener(this);
            ib_mycar_suv.setOnClickListener(this);
            ib_mycar_domestic.setOnClickListener(this);
            ib_mycar_foreign.setOnClickListener(this);
            ib_mycar_auto.setOnClickListener(this);
            ib_mycar_manual.setOnClickListener(this);

        }catch (Exception e){
            e.printStackTrace();
        }
        return mView;
    }

    @Override
    public void onClick(View _view) {
        try {
            if(_view == ib_mycar_goback){
                getActivity().getSupportFragmentManager().beginTransaction().remove(SettingMycarFragment.this).commit();
            }else if(_view == ib_mycar_submit){
                getActivity().getSupportFragmentManager().beginTransaction().remove(SettingMycarFragment.this).commit();
            }else if(_view == ib_mycar_small){
                ib_mycar_small.setImageResource(R.drawable.btn_setting_mycar_small_enable);
                ib_mycar_middle.setImageResource(R.drawable.btn_setting_mycar_middle_disable);
                ib_mycar_large.setImageResource(R.drawable.btn_setting_mycar_large_disable);
                ib_mycar_suv.setImageResource(R.drawable.btn_setting_mycar_suv_disable);
            }else if(_view == ib_mycar_middle){
                ib_mycar_small.setImageResource(R.drawable.btn_setting_mycar_small_disable);
                ib_mycar_middle.setImageResource(R.drawable.btn_setting_mycar_middle_enable);
                ib_mycar_large.setImageResource(R.drawable.btn_setting_mycar_large_disable);
                ib_mycar_suv.setImageResource(R.drawable.btn_setting_mycar_suv_disable);
            }else if(_view == ib_mycar_large){
                ib_mycar_small.setImageResource(R.drawable.btn_setting_mycar_small_disable);
                ib_mycar_middle.setImageResource(R.drawable.btn_setting_mycar_middle_disable);
                ib_mycar_large.setImageResource(R.drawable.btn_setting_mycar_large_enable);
                ib_mycar_suv.setImageResource(R.drawable.btn_setting_mycar_suv_disable);
            }else if(_view == ib_mycar_suv){
                ib_mycar_small.setImageResource(R.drawable.btn_setting_mycar_small_disable);
                ib_mycar_middle.setImageResource(R.drawable.btn_setting_mycar_middle_disable);
                ib_mycar_large.setImageResource(R.drawable.btn_setting_mycar_large_disable);
                ib_mycar_suv.setImageResource(R.drawable.btn_setting_mycar_suv_enable);
            }else if(_view == ib_mycar_domestic){
                ib_mycar_domestic.setImageResource(R.drawable.btn_setting_mycar_domestic_enable);
                ib_mycar_foreign.setImageResource(R.drawable.btn_setting_mycar_foreign_disable);
            }else if(_view == ib_mycar_foreign){
                ib_mycar_domestic.setImageResource(R.drawable.btn_setting_mycar_domestic_disable);
                ib_mycar_foreign.setImageResource(R.drawable.btn_setting_mycar_foreign_enable);
            }else if(_view == ib_mycar_auto){
                ib_mycar_auto.setImageResource(R.drawable.btn_setting_mycar_auto_enable);
                ib_mycar_manual.setImageResource(R.drawable.btn_setting_mycar_manual_disable);
            }else if(_view == ib_mycar_manual){
                ib_mycar_auto.setImageResource(R.drawable.btn_setting_mycar_auto_disable);
                ib_mycar_manual.setImageResource(R.drawable.btn_setting_mycar_manual_enable);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
