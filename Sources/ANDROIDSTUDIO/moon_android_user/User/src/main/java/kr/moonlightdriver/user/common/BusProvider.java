package kr.moonlightdriver.user.common;

import com.squareup.otto.Bus;

/**
 * Created by youngmin on 2016-11-16.
 */

public class BusProvider {
	private static final Bus BUS = new Bus();

	public static Bus getInstance() {
		return BUS;
	}

	private BusProvider() {

	}
}
