package kr.moonlightdriver.user.common;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.Locale;

/**
 * Created by youngmin on 2016-08-18.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String TAG = "DatabaseHelper";
	private static DatabaseHelper mInstance = null;

	public static DatabaseHelper getInstance(Context _context) {
		try {
			mInstance = new DatabaseHelper(_context);

			SQLiteDatabase database = mInstance.getWritableDatabase();
			database.setLocale(Locale.KOREA);

			return mInstance;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private DatabaseHelper(Context _context) {
		super(_context, Configuration.DATABASE_NAME, null, Configuration.DATABASE_VERSION);

		try {
			boolean bResult = isCheckDB(_context);

//			SVUtil.log("error", TAG, "db exist inside : " + bResult);

			if (!bResult) {
				copyDB(_context);
			} else {
				// sdcard에 빼내기
				// copyToSDcardFromDB(_context);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean isCheckDB(Context mContext){
		try {
			String filePath = "/data/data/" + mContext.getPackageName() + "/databases/" + Configuration.DATABASE_NAME;
			File file = new File(filePath);

			if (file.exists()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void copyToSDcardFromDB(Context mContext) {
		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();

			if (sd.canWrite()) {
				String currentDBPath = "/data/" + mContext.getPackageName() + "/databases/" + Configuration.DATABASE_NAME;
				String backupDBPath = Configuration.DATABASE_NAME;
				File currentDB = new File(data, currentDBPath);
				File backupDB = new File(sd, backupDBPath);

				if (currentDB.exists()) {
					FileChannel src = new FileInputStream(currentDB).getChannel();
					FileChannel dst = new FileOutputStream(backupDB).getChannel();
					dst.transferFrom(src, 0, src.size());
					src.close();
					dst.close();
				}
			}

//			SVUtil.log("error", TAG, "copy done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void copyDB(Context _context){
//		SVUtil.log("error", TAG, "copyDB");

		AssetManager manager = _context.getAssets();
		String folderPath = "/data/data/" + _context.getPackageName() + "/databases";
		String filePath = "/data/data/" + _context.getPackageName() + "/databases/" + Configuration.DATABASE_NAME;
		File folder = new File(folderPath);
		File file = new File(filePath);

		FileOutputStream fos = null;
		BufferedOutputStream bos = null;

		try {
			InputStream is = manager.open(Configuration.DATABASE_NAME);
			BufferedInputStream bis = new BufferedInputStream(is);

			if (!folder.exists()) {
				folder.mkdirs();
			}

			if (file.exists()) {
				file.delete();
				file.createNewFile();
			}

			fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos);
			int read = -1;
			byte[] buffer = new byte[1024];

			while ((read = bis.read(buffer, 0, 1024)) != -1) {
				bos.write(buffer, 0, read);
			}

			bos.flush();

			bos.close();
			fos.close();
			bis.close();
			is.close();
		} catch (IOException e) {
			Utils.log("error", TAG, e.getMessage());
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
}
