package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;

/**
 * Created by youngmin on 2016-08-18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateUserLocationAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("crackdownDistance")
	private float mCrackdownDistance;

	public UpdateUserLocationAck() {}

	public UpdateUserLocationAck(ResultData mResultData, float mCrackdownDistance) {
		this.mResultData = mResultData;
		this.mCrackdownDistance = mCrackdownDistance;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public float getmCrackdownDistance() {
		return mCrackdownDistance;
	}

	public void setmCrackdownDistance(float mCrackdownDistance) {
		this.mCrackdownDistance = mCrackdownDistance;
	}
}
