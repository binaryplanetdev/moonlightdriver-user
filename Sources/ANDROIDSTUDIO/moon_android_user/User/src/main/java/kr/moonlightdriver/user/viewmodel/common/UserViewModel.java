package kr.moonlightdriver.user.viewmodel.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2016-06-21.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserViewModel {
	@JsonProperty("userId")
	public String mUserId;

	@JsonProperty("device_uuid")
	public String mDeviceUUID;

	@JsonProperty("gcmId")
	public String mGcmId;

	@JsonProperty("emal")
	public String mEmail;

	@JsonProperty("phone")
	private String mPhoneNumber;

	@JsonProperty("name")
	private String mUserName;

	@JsonProperty("profileImage")
	private String mProfileImage;

	@JsonProperty("locations")
	private CoordinatesData mLocation;

	public UserViewModel() {}

	public UserViewModel(String mDeviceUUID, String mEmail, String mGcmId, CoordinatesData mLocation, String mPhoneNumber, String mProfileImage, String mUserId, String mUserName) {
		this.mDeviceUUID = mDeviceUUID;
		this.mEmail = mEmail;
		this.mGcmId = mGcmId;
		this.mLocation = mLocation;
		this.mPhoneNumber = mPhoneNumber;
		this.mProfileImage = mProfileImage;
		this.mUserId = mUserId;
		this.mUserName = mUserName;
	}

	public String getmDeviceUUID() {
		return mDeviceUUID;
	}

	public void setmDeviceUUID(String mDeviceUUID) {
		this.mDeviceUUID = mDeviceUUID;
	}

	public String getmEmail() {
		return mEmail;
	}

	public void setmEmail(String mEmail) {
		this.mEmail = mEmail;
	}

	public String getmGcmId() {
		return mGcmId;
	}

	public void setmGcmId(String mGcmId) {
		this.mGcmId = mGcmId;
	}

	public CoordinatesData getmLocation() {
		return mLocation;
	}

	public void setmLocation(CoordinatesData mLocation) {
		this.mLocation = mLocation;
	}

	public String getmPhoneNumber() {
		return mPhoneNumber;
	}

	public void setmPhoneNumber(String mPhoneNumber) {
		this.mPhoneNumber = mPhoneNumber;
	}

	public String getmProfileImage() {
		return mProfileImage;
	}

	public void setmProfileImage(String mProfileImage) {
		this.mProfileImage = mProfileImage;
	}

	public String getmUserId() {
		return mUserId;
	}

	public void setmUserId(String mUserId) {
		this.mUserId = mUserId;
	}

	public String getmUserName() {
		return mUserName;
	}

	public void setmUserName(String mUserName) {
		this.mUserName = mUserName;
	}
}
