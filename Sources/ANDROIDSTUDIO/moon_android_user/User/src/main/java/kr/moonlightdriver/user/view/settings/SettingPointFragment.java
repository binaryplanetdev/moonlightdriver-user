package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.view.main.MainActivity;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingPointFragment extends Fragment implements View.OnClickListener{

    private ImageButton ib_setting_goback, ib_setting_point_recharge, btn_recharge_popup_close;
    private View mView;
    private MainActivity mMainActivity;
    private SettingsFragment mFragment;
    LinearLayout popup_point_recharge;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mView = inflater.inflate(R.layout.fragment_setting_point_manage, container, false);

            mMainActivity = (MainActivity)getActivity();

            ib_setting_goback = (ImageButton) mView.findViewById(R.id.ib_setting_goback);
            ib_setting_point_recharge = (ImageButton) mView.findViewById(R.id.ib_setting_point_recharge);
            btn_recharge_popup_close = (ImageButton) mView.findViewById(R.id.btn_recharge_popup_close);

            popup_point_recharge = (LinearLayout) mView.findViewById(R.id.popup_point_recharge);
            popup_point_recharge.setVisibility(View.GONE);

            ib_setting_goback.setOnClickListener(this);
            ib_setting_point_recharge.setOnClickListener(this);
            btn_recharge_popup_close.setOnClickListener(this);

        }catch (Exception e){
            e.printStackTrace();
        }
        return mView;
    }

    @Override
    public void onClick(View _view) {
        try {
            if(_view == ib_setting_goback){
                getActivity().getSupportFragmentManager().beginTransaction().remove(SettingPointFragment.this).commit();
            }else if(_view == ib_setting_point_recharge){
                popup_point_recharge.setVisibility(View.VISIBLE);
            }else if(_view == btn_recharge_popup_close){
                popup_point_recharge.setVisibility(View.GONE);
/*            }else if(_view == ib_mycar_middle){
                ib_mycar_small.setImageResource(R.drawable.btn_setting_mycar_small_disable);
                ib_mycar_middle.setImageResource(R.drawable.btn_setting_mycar_middle_enable);
                ib_mycar_large.setImageResource(R.drawable.btn_setting_mycar_large_disable);
                ib_mycar_suv.setImageResource(R.drawable.btn_setting_mycar_suv_disable);
            }else if(_view == ib_mycar_large){
                ib_mycar_small.setImageResource(R.drawable.btn_setting_mycar_small_disable);
                ib_mycar_middle.setImageResource(R.drawable.btn_setting_mycar_middle_disable);
                ib_mycar_large.setImageResource(R.drawable.btn_setting_mycar_large_enable);
                ib_mycar_suv.setImageResource(R.drawable.btn_setting_mycar_suv_disable);
            }else if(_view == ib_mycar_suv){
                ib_mycar_small.setImageResource(R.drawable.btn_setting_mycar_small_disable);
                ib_mycar_middle.setImageResource(R.drawable.btn_setting_mycar_middle_disable);
                ib_mycar_large.setImageResource(R.drawable.btn_setting_mycar_large_disable);
                ib_mycar_suv.setImageResource(R.drawable.btn_setting_mycar_suv_enable);
            }else if(_view == ib_mycar_domestic){
                ib_mycar_domestic.setImageResource(R.drawable.btn_setting_mycar_domestic_enable);
                ib_mycar_foreign.setImageResource(R.drawable.btn_setting_mycar_foreign_disable);
            }else if(_view == ib_mycar_foreign){
                ib_mycar_domestic.setImageResource(R.drawable.btn_setting_mycar_domestic_disable);
                ib_mycar_foreign.setImageResource(R.drawable.btn_setting_mycar_foreign_enable);
            }else if(_view == ib_mycar_auto){
                ib_mycar_auto.setImageResource(R.drawable.btn_setting_mycar_auto_enable);
                ib_mycar_manual.setImageResource(R.drawable.btn_setting_mycar_manual_disable);
            }else if(_view == ib_mycar_manual){
                ib_mycar_auto.setImageResource(R.drawable.btn_setting_mycar_auto_disable);
                ib_mycar_manual.setImageResource(R.drawable.btn_setting_mycar_manual_enable);
*/            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
