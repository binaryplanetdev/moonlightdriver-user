package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.ChauffeurCompanyViewModel;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChauffeurCompanyListAck extends NetAPI{
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("chauffeur_company_list")
	private ArrayList<ChauffeurCompanyViewModel> mChauffeurCompanyViewModel;
	@JsonProperty("bookmark_list")
	private ArrayList<String> mChauffeurCompanyBookmarkList;

	public ChauffeurCompanyListAck() {}

	public ChauffeurCompanyListAck(ArrayList<String> mChauffeurCompanyBookmarkList, ArrayList<ChauffeurCompanyViewModel> mChauffeurCompanyViewModel, ResultData mResultData) {
		this.mChauffeurCompanyBookmarkList = mChauffeurCompanyBookmarkList;
		this.mChauffeurCompanyViewModel = mChauffeurCompanyViewModel;
		this.mResultData = mResultData;
	}

	public ArrayList<String> getmChauffeurCompanyBookmarkList() {
		return mChauffeurCompanyBookmarkList;
	}

	public void setmChauffeurCompanyBookmarkList(ArrayList<String> mChauffeurCompanyBookmarkList) {
		this.mChauffeurCompanyBookmarkList = mChauffeurCompanyBookmarkList;
	}

	public ArrayList<ChauffeurCompanyViewModel> getmChauffeurCompanyViewModel() {
		return mChauffeurCompanyViewModel;
	}

	public void setmChauffeurCompanyViewModel(ArrayList<ChauffeurCompanyViewModel> mChauffeurCompanyViewModel) {
		this.mChauffeurCompanyViewModel = mChauffeurCompanyViewModel;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
