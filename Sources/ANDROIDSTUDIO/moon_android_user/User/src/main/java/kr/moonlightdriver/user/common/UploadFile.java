package kr.moonlightdriver.user.common;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by youngmin on 2016-11-16.
 */

public class UploadFile {
	private static final String TAG = "UploadFile";
	private int mResponseCode;
	private String mUrlUploadImage;
	private String mFIleName;

	public UploadFile(String _urlUploadImage , String _fileName) {
		this.mUrlUploadImage = _urlUploadImage;
		this.mFIleName = _fileName;
	}

	public String uploadImage() {
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		File sourceFile = new File(this.mFIleName);
		if (!sourceFile.isFile()) {
			Utils.log("error", TAG, "Source File Does not exist!");
			return "{ \"result\" : \"ERROR\" }";
		}

		try {
			FileInputStream fileInputStream = new FileInputStream(sourceFile);
			URL url = new URL(this.mUrlUploadImage);
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			conn.setRequestProperty("uploadFile", this.mFIleName);
			dos = new DataOutputStream(conn.getOutputStream());

			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"uploadFile\";filename=\"" + this.mFIleName + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			Utils.log("error", TAG, "Initial .available : " + bytesAvailable);

			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) {
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			this.mResponseCode = conn.getResponseCode();

			fileInputStream.close();
			dos.flush();
			dos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (this.mResponseCode == 200) {
			StringBuilder sb = new StringBuilder();

			try {
				BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line;

				while ((line = rd.readLine()) != null) {
					sb.append(line);
				}

				rd.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return sb.toString();
		}else {
			Utils.log("error", TAG, "Could not upload : " + this.mResponseCode);
			return "{ \"result\" : \"ERROR\" }";
		}
	}
}
