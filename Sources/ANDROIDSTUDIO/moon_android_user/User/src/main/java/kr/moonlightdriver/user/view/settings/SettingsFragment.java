package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.view.callDriver.CallDriverFragment;
import kr.moonlightdriver.user.view.main.MainActivity;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener{

    private LinearLayout ll_mycar_info, ll_card_manage, ll_point_manage, ll_favorite_manage, ll_run_history, ll_notice;
    private View mView, mCurrentView;
    private MainActivity mMainActivity;
    private CallDriverFragment callDriverFragment;
    private SettingMycarFragment mycarFragment;
    private SettingNoticeFragment noticeFragment;
    private SettingPointFragment pointFragment;
    private SettingHistoryFragment historyFragment;
    private SettingCardFragment cardFragment;
    private SettingFavoriteFragment favoriteFragment;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mView = inflater.inflate(R.layout.fragment_setting, container, false);

            mMainActivity = (MainActivity)getActivity();

            ll_mycar_info = (LinearLayout)mView.findViewById(R.id.ll_mycar_info);
            ll_card_manage = (LinearLayout)mView.findViewById(R.id.ll_card_manage);
            ll_point_manage = (LinearLayout)mView.findViewById(R.id.ll_point_manage);
            ll_favorite_manage = (LinearLayout)mView.findViewById(R.id.ll_favorite_manage);
            ll_run_history = (LinearLayout)mView.findViewById(R.id.ll_run_history);
            ll_notice = (LinearLayout)mView.findViewById(R.id.ll_notice);

            ll_mycar_info.setOnClickListener(this);
            ll_card_manage.setOnClickListener(this);
            ll_point_manage.setOnClickListener(this);
            ll_favorite_manage.setOnClickListener(this);
            ll_run_history.setOnClickListener(this);
            ll_notice.setOnClickListener(this);

        }catch (Exception e){
            e.printStackTrace();
        }
        return mView;
    }

    @Override
    public void onClick(View _view) {
        try {
            switch (_view.getId()){
                case R.id.ll_mycar_info :
                    mycarFragment = new SettingMycarFragment();
                    mMainActivity.addFragment(new SettingMycarFragment());
                    break;
                case R.id.ll_card_manage :
                    cardFragment = new SettingCardFragment();
                    mMainActivity.addFragment(cardFragment);
                    break;
                case R.id.ll_point_manage :
                    pointFragment = new SettingPointFragment();
                    mMainActivity.addFragment(pointFragment);
                    break;
                case R.id.ll_favorite_manage :
                    favoriteFragment = new SettingFavoriteFragment();
                    mMainActivity.addFragment(favoriteFragment);
                    break;
                case R.id.ll_run_history :
                    historyFragment = new SettingHistoryFragment();
                    mMainActivity.addFragment(historyFragment);
                    break;
                case R.id.ll_notice :
                    noticeFragment = new SettingNoticeFragment();
                    mMainActivity.addFragment(noticeFragment);
                    break;
            }

/*            if(_view == ll_mycar_info){

                mycarFragment = new SettingMycarFragment();
                callDriverFragment = new CallDriverFragment();
                //mMainActivity.addFragment(mycarFragment);
                mMainActivity.addFragment(new SettingMycarFragment());

            }else if(_view == ll_card_manage){

                cardFragment = new SettingCardFragment();
                mMainActivity.addFragment(cardFragment);

            }else if(_view == ll_point_manage){

                pointFragment = new SettingPointFragment();
                mMainActivity.addFragment(pointFragment);

            }else if(_view == ll_favorite_manage){

                favoriteFragment = new SettingFavoriteFragment();
                mMainActivity.addFragment(favoriteFragment);

            }else if(_view == ll_run_history){

                historyFragment = new SettingHistoryFragment();
                mMainActivity.addFragment(historyFragment);

            }else if(_view == ll_notice){

                noticeFragment = new SettingNoticeFragment();
                mMainActivity.addFragment(noticeFragment);

            }
            */
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}