package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.CrackdownListViewModel;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrackdownDetailAck extends NetAPI{
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("crackdown_detail")
	private CrackdownListViewModel mCrackdownListViewModel;

	public CrackdownDetailAck() {}

	public CrackdownDetailAck(CrackdownListViewModel mCrackdownListViewModel, ResultData mResultData) {
		this.mCrackdownListViewModel = mCrackdownListViewModel;
		this.mResultData = mResultData;
	}

	public CrackdownListViewModel getmCrackdownListViewModel() {
		return mCrackdownListViewModel;
	}

	public void setmCrackdownListViewModel(CrackdownListViewModel mCrackdownListViewModel) {
		this.mCrackdownListViewModel = mCrackdownListViewModel;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
