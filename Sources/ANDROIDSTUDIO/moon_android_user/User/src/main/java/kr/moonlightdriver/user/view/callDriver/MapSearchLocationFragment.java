package kr.moonlightdriver.user.view.callDriver;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapSearchLocationFragment extends Fragment implements OnMapReadyCallback {
	private static final String TAG = "MapSearchLocationFragment";
	private final float DEFAULT_ZOOM  = 16f;
	private MainActivity mMainActivity;

	private String mLocationType;

	private GoogleMap mGoogleMap;

	private View mView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.fragment_map_search_location, container, false);
			mMainActivity = (MainActivity) getActivity();
			mMainActivity.setTabMenuVisibility(View.GONE);

			Bundle bundle = MapSearchLocationFragment.this.getArguments();
			mLocationType = bundle.getString("location_type", "");

			setEventListener();

			setUpMapIfNeeded();
		} catch(Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.search_map);
				mapFragment.getMapAsync(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEventListener() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendResult(LocationData _data) {
		Intent intent = new Intent();
		intent.putExtra("locationType", mLocationType);
		intent.putExtra("locationName", _data.getmLocationName());
		intent.putExtra("address", _data.getmAddress());
		intent.putExtra("lat", _data.getmLocation().latitude);
		intent.putExtra("lng", _data.getmLocation().longitude);

		getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
	}

	private void closeFragment() {
		try {
			getChildFragmentManager().beginTransaction().remove(MapSearchLocationFragment.this).commit();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		try {
			mGoogleMap = googleMap;

			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);

			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMainActivity.mCurrentLocation, DEFAULT_ZOOM));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			mMainActivity.setTabMenuVisibility(View.VISIBLE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
