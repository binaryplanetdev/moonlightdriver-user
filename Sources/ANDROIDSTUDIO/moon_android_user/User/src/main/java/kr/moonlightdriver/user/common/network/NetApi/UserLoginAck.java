package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.common.UserAdditionalViewModel;
import kr.moonlightdriver.user.viewmodel.common.UserViewModel;

/**
 * Created by youngmin on 2016-08-18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserLoginAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("userDetail")
	private UserViewModel mUserDetailData;

	@JsonProperty("userAdditional")
	private UserAdditionalViewModel mUserAdditionalData;

	public UserLoginAck() {}

	public UserLoginAck(ResultData mResultData, UserViewModel mUserDetailData, UserAdditionalViewModel mUserAdditionalData) {
		this.mResultData = mResultData;
		this.mUserDetailData = mUserDetailData;
		this.mUserAdditionalData = mUserAdditionalData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public UserViewModel getmUserDetailData() {
		return mUserDetailData;
	}

	public void setmUserDetailData(UserViewModel mUserDetailData) {
		this.mUserDetailData = mUserDetailData;
	}

	public UserAdditionalViewModel getmUserAdditionalData() {
		return mUserAdditionalData;
	}

	public void setmUserAdditionalData(UserAdditionalViewModel mUserAdditionalData) {
		this.mUserAdditionalData = mUserAdditionalData;
	}
}
