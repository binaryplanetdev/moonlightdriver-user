package kr.moonlightdriver.user.common;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.view.main.NoticePushActivity;

/**
 * Created by youngmin on 2016-10-31.
 */

public class GcmIntentService extends IntentService {
	private static final String TAG = "GcmIntentService";
	public static final int NOTIFICATION_ID = 1;

	public GcmIntentService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		try {
			Bundle extras = intent.getExtras();

			if(extras != null) {
				GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

				String messageType = gcm.getMessageType(intent);

				if (!extras.isEmpty()) {
			        Utils.log("error", TAG, "Received: " + extras.toString());

					if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
						sendNotification(extras);
					}
				}

				GcmBroadcastReceiver.completeWakefulIntent(intent);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void sendNotification(Bundle _args) {
		try {
			String push_type = _args.getString("type", "");

			String title = getNotificationTitle(push_type);
			if(title.isEmpty()) {
				return;
			}

			title = _args.getString("title", title);

			String imgUrl = _args.getString("imgUrl", "");
			Bitmap notifyImg = null;
			if(!imgUrl.isEmpty()) {
				notifyImg = Utils.getImageDownload(Configuration.IMAGE_SERVER_HOST + imgUrl);
			}

			String content = _args.getString("message", "");
			Intent intent = getIntent(_args);

			PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
			builder.setSmallIcon(R.drawable.ic_launcher);
			builder.setWhen(System.currentTimeMillis());

			if(push_type.equals("crackdown_warning")) {
				builder.setTicker(content);
				RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.crackdown_notification);

				int distance = Integer.parseInt(_args.getString("distance", "0"));
				if(distance == 300) {
					contentView.setImageViewResource(R.id.crackdown_notification_image, R.drawable.push_notice_300m);
				} else if(distance == 500) {
					contentView.setImageViewResource(R.id.crackdown_notification_image, R.drawable.push_notice_500m);
				} else if(distance == 1000) {
					contentView.setImageViewResource(R.id.crackdown_notification_image, R.drawable.push_notice_1000m);
				}

				builder.setCustomHeadsUpContentView(contentView);
			} else {
				builder.setTicker(title);
			}

			builder.setContentTitle(title);
			builder.setContentText(content);
			builder.setContentIntent(contentIntent);
			builder.setAutoCancel(true);
			Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
			builder.setLargeIcon(bm);
			builder.setVibrate(getVibratePattern(_args));
			builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

			if(Build.VERSION.SDK_INT > 15) {
				builder.setPriority(Notification.PRIORITY_MAX);
			}

			if(notifyImg != null) {
				NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
				style.setBigContentTitle(title);
				style.setSummaryText(content);
				style.bigPicture(notifyImg);
				builder.setStyle(style);
			} else {
				NotificationCompat.BigTextStyle textStyle = new NotificationCompat.BigTextStyle();
				textStyle.setBigContentTitle(title);
				textStyle.bigText(content);
				textStyle.setSummaryText(content);
				builder.setStyle(textStyle);
			}

			NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
			if(Build.VERSION.SDK_INT > 15) {
				notificationManager.notify(NOTIFICATION_ID, builder.build());
			} else {
				notificationManager.notify(NOTIFICATION_ID, builder.getNotification());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private long[] getVibratePattern(Bundle _args) {
		try {
			String type = _args.getString("type", "");

			if(!type.isEmpty()) {
				switch (type) {
					case "crackdown_warning" :
						int distance = Integer.parseInt(_args.getString("distance", "0"));

						if(distance == 300) {
							return new long[] { 200, 1000, 200, 1000, 200, 1000, 200, 1000, 200, 1000 };
						} else if(distance == 500) {
							return new long[] { 200, 1000, 200, 1000, 200, 1000 };
						} else if(distance == 1000) {
							return new long[] { 200, 2000 };
						}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new long[] { 200, 1000, 200, 1000 };
	}

	private String getNotificationTitle(String _messageType) {
		try {
			switch (_messageType) {
				case "notice" :
					return "공지사항";
				case "crackdown_warning":
					return "음주단속 알림";
				default :
					return "";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private Intent getIntent(Bundle _args) {
		Intent intent = new Intent(this, NoticePushActivity.class);

		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		try {
			String type = _args.getString("type", "");

			if(!type.isEmpty()) {
				switch (type) {
					case "notice" :
						intent.putExtra("messageType", type);
						intent.putExtra("title", _args.getString("title", ""));
						intent.putExtra("message", _args.getString("message", ""));
						intent.putExtra("imgUrl", _args.getString("imgUrl", ""));
						break;
					case "crackdown_warning" :
						intent = new Intent(this, MainActivity.class);
						intent.putExtra("messageType", type);
						break;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return intent;
	}
}
