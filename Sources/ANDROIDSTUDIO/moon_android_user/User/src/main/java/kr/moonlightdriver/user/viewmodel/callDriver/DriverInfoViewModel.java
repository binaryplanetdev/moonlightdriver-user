package kr.moonlightdriver.user.viewmodel.callDriver;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by youngmin on 2016-06-21.
 */
public class DriverInfoViewModel {
	private String mDriverName;
	private String mDriverPhoneNumber;
	private int mDriverStar;
	private String mDriverPhoto;
	private LatLng mLocation;

	public DriverInfoViewModel() {}

	public String getmDriverName() {
		return mDriverName;
	}

	public void setmDriverName(String mDriverName) {
		this.mDriverName = mDriverName;
	}

	public String getmDriverPhoneNumber() {
		return mDriverPhoneNumber;
	}

	public void setmDriverPhoneNumber(String mDriverPhoneNumber) {
		this.mDriverPhoneNumber = mDriverPhoneNumber;
	}

	public String getmDriverPhoto() {
		return mDriverPhoto;
	}

	public void setmDriverPhoto(String mDriverPhoto) {
		this.mDriverPhoto = mDriverPhoto;
	}

	public int getmDriverStar() {
		return mDriverStar;
	}

	public void setmDriverStar(int mDriverStar) {
		this.mDriverStar = mDriverStar;
	}

	public LatLng getmLocation() {
		return mLocation;
	}

	public void setmLocation(LatLng mLocation) {
		this.mLocation = mLocation;
	}
}
