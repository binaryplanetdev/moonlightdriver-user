package kr.moonlightdriver.user.viewmodel.crackdown;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2016-11-24.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrackdownRankListViewModel {
    @JsonProperty("rank")
    private int mRank;

    @JsonProperty("registrant")
    private String mRegistrant;

    @JsonProperty("reportCount")
    private int mReportCount;

    @JsonProperty("reportPoint")
    private int mReportPoint;

    @JsonProperty("reportType")
    private String mReportType;

    public CrackdownRankListViewModel() { }

    public CrackdownRankListViewModel(int mRank, String mRegistrant, int mReportCount, int mReportPoint, String mReportType) {
        this.mRank = mRank;
        this.mRegistrant = mRegistrant;
        this.mReportCount = mReportCount;
        this.mReportPoint = mReportPoint;
        this.mReportType = mReportType;
    }

    public String getmReportType() {
        return mReportType;
    }

    public void setmReportType(String mReportType) {
        this.mReportType = mReportType;
    }

    public int getmRank() {
        return mRank;
    }

    public void setmRank(int mRank) {
        this.mRank = mRank;
    }

    public String getmRegistrant() {
        return mRegistrant;
    }

    public void setmRegistrant(String mRegistrant) {
        this.mRegistrant = mRegistrant;
    }

    public int getmReportCount() {
        return mReportCount;
    }

    public void setmReportCount(int mReportCount) {
        this.mReportCount = mReportCount;
    }

    public int getmReportPoint() {
        return mReportPoint;
    }

    public void setmReportPoint(int mReportPoint) {
        this.mReportPoint = mReportPoint;
    }
}
