package kr.moonlightdriver.user.viewmodel.callDriver;

import kr.moonlightdriver.user.viewmodel.common.LocationData;

/**
 * Created by youngmin on 2016-07-01.
 */
public class StopByViewModel {
	private String mTitle;
	private LocationData mStopBy;

	public StopByViewModel() {}

	public StopByViewModel(String mTitle) {
		this.mTitle = mTitle;
	}

	public StopByViewModel(LocationData mStopBy, String mTitle) {
		this.mStopBy = mStopBy;
		this.mTitle = mTitle;
	}

	public LocationData getmStopBy() {
		return mStopBy;
	}

	public void setmStopBy(LocationData mStopBy) {
		this.mStopBy = mStopBy;
	}

	public String getmTitle() {
		return mTitle;
	}

	public void setmTitle(String mTitle) {
		this.mTitle = mTitle;
	}
}
