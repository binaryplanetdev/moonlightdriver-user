package kr.moonlightdriver.user.viewmodel.common;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by youngmin on 2016-06-21.
 */
public class LocationData {
	private LatLng mLocation;
	private String mLocationName;
	private String mAddress;

	public LocationData() {

	}

	public LocationData(String mAddress, LatLng mLocation, String mLocationName) {
		this.mAddress = mAddress;
		this.mLocation = mLocation;
		this.mLocationName = mLocationName;
	}

	public String getmAddress() {
		return mAddress;
	}

	public void setmAddress(String mAddress) {
		this.mAddress = mAddress;
	}

	public LatLng getmLocation() {
		return mLocation;
	}

	public void setmLocation(LatLng mLocation) {
		this.mLocation = mLocation;
	}

	public String getmLocationName() {
		return mLocationName;
	}

	public void setmLocationName(String mLocationName) {
		this.mLocationName = mLocationName;
	}
}
