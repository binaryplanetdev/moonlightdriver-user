package kr.moonlightdriver.user.viewholder;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by youngmin on 2016-08-30.
 */
public class ChauffeurCompanyListViewHolder {
	public LinearLayout mBtnChauffeurCompanyBookmarkLayout;
	public ImageView mBtnChauffeurCompanyBookmark;
	public TextView mChauffeurCompanyName;
	public LinearLayout mBtnChauffeurCompanySecretCallLayout;
	public ImageView mBtnChauffeurCompanySecretCall;
}
