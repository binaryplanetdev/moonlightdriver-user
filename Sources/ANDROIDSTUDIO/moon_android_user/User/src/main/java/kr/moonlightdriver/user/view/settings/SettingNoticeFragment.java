package kr.moonlightdriver.user.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.view.main.MainActivity;

/**
 * Created by youngmin on 2016-06-21.
 */
public class SettingNoticeFragment extends Fragment implements View.OnClickListener{

    private ImageButton ib_setting_goback;
    private View mView;
    private MainActivity mMainActivity;
    private SettingsFragment mFragment;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            mView = inflater.inflate(R.layout.fragment_setting_notice, container, false);

            mMainActivity = (MainActivity)getActivity();

            ib_setting_goback = (ImageButton) mView.findViewById(R.id.ib_setting_goback);

            ib_setting_goback.setOnClickListener(this);

        }catch (Exception e){
            e.printStackTrace();
        }
        return mView;
    }

    @Override
    public void onClick(View _view) {
        try {
            if(_view == ib_setting_goback){
                Utils.log("error", Utils.TAG, ">>>>" + ib_setting_goback.toString());
                getActivity().getSupportFragmentManager().beginTransaction().remove(SettingNoticeFragment.this).commit();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
