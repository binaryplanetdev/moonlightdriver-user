package kr.moonlightdriver.user.viewmodel.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2016-12-29.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class NoticeData {
    @JsonProperty("userNoticeId")
    public String mUserNoticeId;

    @JsonProperty("seq")
    public int mSeq;

    @JsonProperty("title")
    public String mTitle;

    @JsonProperty("contents")
    public String mContents;

    @JsonProperty("imagePath")
    public String mImagePath;

    @JsonProperty("orgImagePath")
    public String mOrgImagePath;

    @JsonProperty("linkUrl")
    public String mLinkUrl;

    @JsonProperty("startDate")
    public long mStartDate;

    @JsonProperty("endDate")
    public long mEndDate;

    public NoticeData() {    }

    public NoticeData(String mUserNoticeId, int mSeq, String mTitle, String mContents, String mImagePath, String mOrgImagePath, String mLinkUrl, long mStartDate, long mEndDate) {
        this.mUserNoticeId = mUserNoticeId;
        this.mSeq = mSeq;
        this.mTitle = mTitle;
        this.mContents = mContents;
        this.mImagePath = mImagePath;
        this.mOrgImagePath = mOrgImagePath;
        this.mLinkUrl = mLinkUrl;
        this.mStartDate = mStartDate;
        this.mEndDate = mEndDate;
    }

    public String getmImagePath() {
        return mImagePath;
    }

    public void setmImagePath(String mImagePath) {
        this.mImagePath = mImagePath;
    }

    public String getmLinkUrl() {
        return mLinkUrl;
    }

    public void setmLinkUrl(String mLinkUrl) {
        this.mLinkUrl = mLinkUrl;
    }

    public String getmUserNoticeId() {
        return mUserNoticeId;
    }

    public void setmUserNoticeId(String mUserNoticeId) {
        this.mUserNoticeId = mUserNoticeId;
    }

    public int getmSeq() {
        return mSeq;
    }

    public void setmSeq(int mSeq) {
        this.mSeq = mSeq;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmContents() {
        return mContents;
    }

    public void setmContents(String mContents) {
        this.mContents = mContents;
    }

    public String getmOrgImagePath() {
        return mOrgImagePath;
    }

    public void setmOrgImagePath(String mOrgImagePath) {
        this.mOrgImagePath = mOrgImagePath;
    }

    public long getmStartDate() {
        return mStartDate;
    }

    public void setmStartDate(long mStartDate) {
        this.mStartDate = mStartDate;
    }

    public long getmEndDate() {
        return mEndDate;
    }

    public void setmEndDate(long mEndDate) {
        this.mEndDate = mEndDate;
    }
}
