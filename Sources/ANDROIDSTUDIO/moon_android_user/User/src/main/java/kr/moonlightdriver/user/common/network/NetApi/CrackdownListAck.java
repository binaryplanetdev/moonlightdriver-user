package kr.moonlightdriver.user.common.network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.crackdown.CrackdownListViewModel;

/**
 * Created by youngmin on 2016-08-11.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CrackdownListAck extends NetAPI{
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("crackdown_list")
	private ArrayList<CrackdownListViewModel> mCrackdownListViewModel;

	public CrackdownListAck() {}

	public CrackdownListAck(ArrayList<CrackdownListViewModel> mCrackdownListViewModel, ResultData mResultData) {
		this.mCrackdownListViewModel = mCrackdownListViewModel;
		this.mResultData = mResultData;
	}

	public ArrayList<CrackdownListViewModel> getmCrackdownListViewModel() {
		return mCrackdownListViewModel;
	}

	public void setmCrackdownListViewModel(ArrayList<CrackdownListViewModel> mCrackdownListViewModel) {
		this.mCrackdownListViewModel = mCrackdownListViewModel;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
