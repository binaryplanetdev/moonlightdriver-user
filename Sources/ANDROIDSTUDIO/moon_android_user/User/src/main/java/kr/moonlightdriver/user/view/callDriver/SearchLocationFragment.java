package kr.moonlightdriver.user.view.callDriver;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewholder.SearchLocationListViewHolder;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchLocationFragment extends Fragment {
	private static final String TAG = "SearchLocationFragment";
	private MainActivity mMainActivity;

	private String mLocationType;

	private TextView mTextViewSearchTitle;
	private EditText mEditTextSearchLocation;
	private TextView mTextViewCurrentLocation;
	private Button mBtnSearchLocation;
	private ListView mListViewSearchResultList;
	private LinearLayout mCurrentLocationLayout;
	private LinearLayout mMapLocationLayout;

	private ArrayList<LocationData> mSearchResultList;
	private SearchLocationListAdapter mSearchResultAdapter;

	private View mView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.fragment_search_location, container, false);
			mMainActivity = (MainActivity) getActivity();
			mMainActivity.setTabMenuVisibility(View.GONE);

			mTextViewSearchTitle = (TextView) mView.findViewById(R.id.textview_search_title);
			mEditTextSearchLocation = (EditText) mView.findViewById(R.id.edittext_search_location);
			mTextViewCurrentLocation = (TextView) mView.findViewById(R.id.textview_current_location);
			mBtnSearchLocation = (Button) mView.findViewById(R.id.btnSearchLocation);
			mListViewSearchResultList = (ListView) mView.findViewById(R.id.search_result_list);
			mCurrentLocationLayout = (LinearLayout) mView.findViewById(R.id.current_location_layout);
			mMapLocationLayout = (LinearLayout) mView.findViewById(R.id.map_location_layout);

			mSearchResultList = new ArrayList<>();
			mSearchResultAdapter = new SearchLocationListAdapter(mMainActivity, R.layout.row_search_location_list, mSearchResultList);
			mListViewSearchResultList.setAdapter(mSearchResultAdapter);

			Bundle bundle = SearchLocationFragment.this.getArguments();
			mLocationType = bundle.getString("location_type", "");

			setEventListener();
			setSearchTitle();
			getSearchHistory();
		} catch(Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void setEventListener() {
		try {
			mBtnSearchLocation.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

				}
			});

			mCurrentLocationLayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {

				}
			});

			mMapLocationLayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					MapSearchLocationFragment mapFragment = new MapSearchLocationFragment();

					Bundle args = new Bundle();
					args.putString("locationType", mLocationType);

					mapFragment.setTargetFragment(getTargetFragment(), 1);

					mMainActivity.replaceFragment(mapFragment);
				}
			});

			mListViewSearchResultList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> _adapterView, View _view, int _position, long _id) {
					try {
						LocationData data = mSearchResultList.get(_position);
						if(data != null) {
							sendResult(data);
						}

						closeFragment();
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void closeFragment() {
		getChildFragmentManager().beginTransaction().remove(SearchLocationFragment.this).commit();
	}

	private void sendResult(LocationData _data) {
		Intent intent = new Intent();
		intent.putExtra("locationType", mLocationType);
		intent.putExtra("locationName", _data.getmLocationName());
		intent.putExtra("address", _data.getmAddress());
		intent.putExtra("lat", _data.getmLocation().latitude);
		intent.putExtra("lng", _data.getmLocation().longitude);

		getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
	}

	private void getSearchHistory() {
		try {
			if(mLocationType.equals("departure")) {
				for(int i = 0; i < 20; i++) {
					LocationData tmp = new LocationData();
					tmp.setmLocationName("출발" + i);
					tmp.setmAddress("서울시 강남구 출발" + i);
					tmp.setmLocation(new LatLng(37.5531073, 126.9822973));

					mSearchResultList.add(tmp);
				}
			} else if(mLocationType.equals("arrive")) {
				for(int i = 0; i < 15; i++) {
					LocationData tmp = new LocationData();
					tmp.setmLocationName("도착" + i);
					tmp.setmAddress("서울시 강남구 도착" + i);
					tmp.setmLocation(new LatLng(37.5531073, 126.9822973));

					mSearchResultList.add(tmp);
				}
			} else if(mLocationType.startsWith("stopby")) {
				for(int i = 0; i < 10; i++) {
					LocationData tmp = new LocationData();
					tmp.setmLocationName("경유" + i);
					tmp.setmAddress("서울시 강남구 경유" + i);
					tmp.setmLocation(new LatLng(37.5531073, 126.9822973));

					mSearchResultList.add(tmp);
				}
			}

			mSearchResultAdapter.notifyDataSetChanged();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void setSearchTitle() {
		try {
			if(mLocationType.equals("departure")) {
				mTextViewSearchTitle.setText("출발");
				mEditTextSearchLocation.setHint("출발지 검색");
				mTextViewCurrentLocation.setText("를 출발지로 설정하기");
			} else if(mLocationType.equals("arrive")) {
				mTextViewSearchTitle.setText("도착");
				mEditTextSearchLocation.setHint("목적지 검색");
				mTextViewCurrentLocation.setText("를 목적지로 설정하기");
			} else if(mLocationType.startsWith("stopby")) {
				mTextViewSearchTitle.setText("경유");
				mEditTextSearchLocation.setHint("경유지 검색");
				mTextViewCurrentLocation.setText("를 경유지로 설정하기");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class SearchLocationListAdapter extends ArrayAdapter<LocationData> {
		private ArrayList<LocationData> mItems;
		private int mLayoutID;
		private SearchLocationListViewHolder mViewHolder;

		public SearchLocationListAdapter(Context _context, int _layoutID, ArrayList<LocationData> _items) {
			super(_context, _layoutID, _items);

			this.mItems = _items;
			this.mLayoutID = _layoutID;
		}

		@Override
		public View getView(int _position, View _convertView, ViewGroup _parent) {
			View view = _convertView;

			try {
				if(view == null) {
					LayoutInflater inflater = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					view = inflater.inflate(mLayoutID, null);

					mViewHolder = new SearchLocationListViewHolder();

					mViewHolder.mTextViewLocationName = (TextView) view.findViewById(R.id.row_search_list_location_name);
					mViewHolder.mTextViewAddress = (TextView) view.findViewById(R.id.row_search_list_address);

					view.setTag(mViewHolder);
				} else {
					mViewHolder = (SearchLocationListViewHolder) view.getTag();
				}

				LocationData locationData = mItems.get(_position);
				mViewHolder.mTextViewLocationName.setText(locationData.getmLocationName());
				mViewHolder.mTextViewAddress.setText(locationData.getmAddress());
			} catch (Exception e) {
				e.printStackTrace();
			}

			return view;
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			mMainActivity.setTabMenuVisibility(View.VISIBLE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
