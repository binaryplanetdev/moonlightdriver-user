package kr.moonlightdriver.user.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import kr.moonlightdriver.user.BuildConfig;
import kr.moonlightdriver.user.view.main.MainActivity;

public class Utils {
	public static final String TAG = "Utils";

	public static int GetLocationServiceStatus(Context cx) {
		try {
			LocationManager lm = (LocationManager) cx.getSystemService(Context.LOCATION_SERVICE);
			if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) && lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				return 3;
			} else if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				return 2;
			} else if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				return 1;
			} else { // 위치 찾기 불가
				return 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static boolean GetWifiOnStatus(Context cx) {
		try {
			ConnectivityManager cm = (ConnectivityManager)cx.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
			if(activeNetwork != null) {
				if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
					return activeNetwork.isAvailable();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public static void log(String _logType, String _tag, String _message) {
		try {
			if(BuildConfig.DEBUG) {
				switch (_logType.toLowerCase()) {
					case "verbose" :
						Log.v(_tag, _message);
						break;
					case "debug" :
						Log.d(_tag, _message);
						break;
					case "info" :
						Log.i(_tag, _message);
						break;
					case "warning" :
						Log.w(_tag, _message);
						break;
					case "error" :
						Log.e(_tag, _message);
						break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	public static int dpFromPx(final Context context, final float px) {
//		return Math.round(px / context.getResources().getDisplayMetrics().density);
//	}

	public static int pxFromDp(final Context context, final float dp) {
		return Math.round(dp * context.getResources().getDisplayMetrics().density);
	}

	public static void finishActivity(Activity _activity) {
		try {
			_activity.moveTaskToBack(true);
			_activity.finish();
			android.os.Process.killProcess(android.os.Process.myPid());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getCommentRegisteredDate(long _createTime) {
		String regDateStr = "";

		try {
			Calendar regDate = Calendar.getInstance();
			regDate.setTimeInMillis(_createTime);

			int raw_month = regDate.get(Calendar.MONTH) + 1;

			String year = regDate.get(Calendar.YEAR) + "-";
			String month = (raw_month < 10 ? "0" + raw_month : raw_month) + "-";
			String dayofmonth = (regDate.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + regDate.get(Calendar.DAY_OF_MONTH) : regDate.get(Calendar.DAY_OF_MONTH)) + " ";
			String hours = (regDate.get(Calendar.HOUR) < 10 ? "0" + regDate.get(Calendar.HOUR) : regDate.get(Calendar.HOUR)) + ":";
			String minutes = (regDate.get(Calendar.MINUTE) < 10 ? "0" + regDate.get(Calendar.MINUTE) : regDate.get(Calendar.MINUTE)) + "" + (regDate.get(Calendar.HOUR_OF_DAY) < 12 ? "AM" : "PM");

			regDateStr = year + month + dayofmonth + hours + minutes;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return regDateStr;
	}

	public static String getElapsedTime(long _createTime) {
		String elapsedTime = "";

		try {
			long minutes = 60 * 1000;
			long hours = 60 * 60 * 1000;
			long days = 24 * 60 * 60 * 1000;
			long now = System.currentTimeMillis();
			long diff = now - _createTime;

			if(diff < minutes) {
				elapsedTime = "방금 전";
			} else if(diff >= minutes && diff < hours) {
				elapsedTime = ((int)(diff / minutes)) + "분 전";
			} else if(diff >= hours && diff < days) {
				elapsedTime = ((int)(diff / hours)) + "시간 전";
			} else if(diff >= days) {
				elapsedTime = ((int)(diff / days)) + "일 전";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return elapsedTime;
	}

	public static void setFontToView(Typeface _type_face, TextView... views) {
		for (TextView view : views)
			view.setTypeface(_type_face);
	}

	public static SharedPreferences getGCMPreferences(Context _context) {
		return _context.getSharedPreferences(Configuration.PREFERENCES_NAME, Context.MODE_PRIVATE);
	}

	public static String getDeviceUUID(final Context _context) {
		String uuid_str = "";

		try {
			final SharedPreferences prefs = Utils.getGCMPreferences(_context);
			final String deviceUUID = prefs.getString(Configuration.PROPERTY_DEVICE_UUID, "");

			UUID uuid = null;
			if (!deviceUUID.isEmpty()) {
				uuid = UUID.fromString(deviceUUID);
			} else {
				final String androidId = Settings.Secure.getString(_context.getContentResolver(), Settings.Secure.ANDROID_ID);
				if (!"9774d56d682e549c".equals(androidId)) {
					uuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
				} else {
					final String deviceId = ((TelephonyManager) _context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
					uuid = deviceId != null ? UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")) : UUID.randomUUID();
				}

				SharedPreferences.Editor editor = prefs.edit();
				editor.putString(Configuration.PROPERTY_DEVICE_UUID, uuid.toString());
				editor.apply();
			}

			uuid_str = uuid.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return uuid_str;
	}

	public static String getCrackdownRegistrantEncrypt(String _registrant) {
		String retRegistrant = "";

		try {
			StringBuilder replacedRegistrant = new StringBuilder(_registrant);
			replacedRegistrant.setCharAt(1, 'x');
			replacedRegistrant.setCharAt(2, 'x');

			retRegistrant = replacedRegistrant.toString();
			retRegistrant = retRegistrant.substring(0, 4) + "-" + retRegistrant.substring(4);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return retRegistrant;
	}

	public static void hideSoftInput(MainActivity _mainActivity) {
		try {
			View view = _mainActivity.getCurrentFocus();
			if(view != null) {
				InputMethodManager imm = (InputMethodManager) _mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void initRegion() {
		try {
			Configuration.REGION = new JSONObject();

			Configuration.REGION.put("서울특별시", "서울경기");
			Configuration.REGION.put("경기도", "서울경기");
			Configuration.REGION.put("인천광역시", "서울경기");
			Configuration.REGION.put("강원도", "강원도");
			Configuration.REGION.put("충청도", "충청도");
			Configuration.REGION.put("충청남도", "충청도");
			Configuration.REGION.put("충청북도", "충청도");
			Configuration.REGION.put("대전광역시", "충청도");
			Configuration.REGION.put("세종특별자치시", "충청도");
			Configuration.REGION.put("경상도", "경상도");
			Configuration.REGION.put("경상남도", "경상도");
			Configuration.REGION.put("경상북도", "경상도");
			Configuration.REGION.put("울산광역시", "경상도");
			Configuration.REGION.put("부산광역시", "경상도");
			Configuration.REGION.put("전라도", "전라도");
			Configuration.REGION.put("전라남도", "전라도");
			Configuration.REGION.put("전라북도", "전라도");
			Configuration.REGION.put("광주광역시", "전라도");
			Configuration.REGION.put("제주도", "제주도");
			Configuration.REGION.put("제주특별자치도", "제주도");
			Configuration.REGION.put("대구광역시", "대구");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<ResolveInfo> getShareIntents(PackageManager _pm, String... _appNameList) {
		ArrayList<ResolveInfo> retSharedAppInfo = new ArrayList<>();

		try {
			for (String appName : _appNameList) {
				Intent intent = new Intent(android.content.Intent.ACTION_SEND);
				intent.setType("image/png");

				List<ResolveInfo> resInfo = _pm.queryIntentActivities(intent, 0);

				if(resInfo == null) {
					return new ArrayList<>();
				}

				for (ResolveInfo info : resInfo) {
					if (info.activityInfo.packageName.toLowerCase().contains(appName) || info.activityInfo.name.toLowerCase().contains(appName) ) {
						retSharedAppInfo.add(info);
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return retSharedAppInfo;
	}

	public static String saveScreenshot(Bitmap _bitmap) {
		String saveFileName = "";

		try {
			Utils.log("error", TAG, "Environment.getExternalStorageDirectory(); : " + Environment.getExternalStorageDirectory());

			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
			String dateString = formatter.format(new Date());
			String fileName = "/moonlightdriver_shared_" + dateString + ".png";
			File dirs = new File(Environment.getExternalStorageDirectory(), Configuration.PATH_SHARED_IMAGE);

			if (!dirs.exists()) {
				dirs.mkdirs();
			}

			saveFileName = dirs.getPath() + fileName;

			FileOutputStream outStream  = new FileOutputStream(saveFileName);
			_bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);

			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return saveFileName;
	}

	public static String convertToPriceString(int _number) {
		try {
			DecimalFormat df = new DecimalFormat("#,##0");
			return df.format(_number) + "원";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String url = urls[0];
			Bitmap mIcon11 = null;

			try {
				InputStream in = new java.net.URL(url).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			if(result != null) {
				bmImage.setImageBitmap(result);
				this.bmImage.setVisibility(View.VISIBLE);
			}
		}
	}

	public static Bitmap getImageDownload(String _imgUrl) {
		Bitmap img = null;
		try {
			URL url = new URL(_imgUrl);
			URLConnection conn = url.openConnection();
			conn.connect();

			BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
			img = BitmapFactory.decodeStream(bis);
			bis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return img;
	}

	public static long getLocationUpdateTime(double _kmPerHour) {
		try {
			if(_kmPerHour >= 40) {
				return 1000;
			} else if(_kmPerHour >= 30) {
				return 1200;
			} else if(_kmPerHour >= 25) {
				return 1400;
			} else if(_kmPerHour >= 20) {
				return 1800;
			} else if(_kmPerHour >= 19) {
				return 1900;
			} else if(_kmPerHour >= 18) {
				return 2000;
			} else if(_kmPerHour >= 17) {
				return 2100;
			} else if(_kmPerHour >= 16) {
				return 2200;
			} else if(_kmPerHour >= 15) {
				return 2400;
			} else if(_kmPerHour >= 14) {
				return 2600;
			} else if(_kmPerHour >= 13) {
				return 2800;
			} else if(_kmPerHour >= 12) {
				return 3000;
			} else if(_kmPerHour >= 11) {
				return 3200;
			} else if(_kmPerHour >= 10) {
				return 3600;
			} else if(_kmPerHour >= 9) {
				return 4000;
			} else if(_kmPerHour >= 8) {
				return 4500;
			} else if(_kmPerHour >= 7) {
				return 5000;
			} else if(_kmPerHour >= 6) {
				return 6000;
			} else if(_kmPerHour >= 5) {
				return 7000;
			} else if(_kmPerHour >= 4) {
				return 9000;
			} else if(_kmPerHour >= 3) {
				return 12000;
			} else if(_kmPerHour >= 2) {
				return 18000;
			} else if(_kmPerHour >= 1) {
				return 36000;
			} else {
				return 60000;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 60000;
		}
	}
}
