package kr.moonlightdriver.user.viewmodel.callDriver;

import kr.moonlightdriver.user.viewmodel.common.LocationData;

/**
 * Created by youngmin on 2016-06-21.
 */
public class CallDriverViewModel {
	private LocationData mDepartFrom;
	private LocationData mArriveAt;

	private float mDistance;
	private int mTime;
	private int mPrice;
	private String mPayMethod;

	private DriverInfoViewModel mDriverInfo;

	public CallDriverViewModel() {}

	public LocationData getmArriveAt() {
		return mArriveAt;
	}

	public void setmArriveAt(LocationData mArriveAt) {
		this.mArriveAt = mArriveAt;
	}

	public LocationData getmDepartFrom() {
		return mDepartFrom;
	}

	public void setmDepartFrom(LocationData mDepartFrom) {
		this.mDepartFrom = mDepartFrom;
	}

	public float getmDistance() {
		return mDistance;
	}

	public void setmDistance(float mDistance) {
		this.mDistance = mDistance;
	}

	public String getmPayMethod() {
		return mPayMethod;
	}

	public void setmPayMethod(String mPayMethod) {
		this.mPayMethod = mPayMethod;
	}

	public int getmPrice() {
		return mPrice;
	}

	public void setmPrice(int mPrice) {
		this.mPrice = mPrice;
	}

	public int getmTime() {
		return mTime;
	}

	public void setmTime(int mTime) {
		this.mTime = mTime;
	}

	public DriverInfoViewModel getmDriverInfo() {
		return mDriverInfo;
	}

	public void setmDriverInfo(DriverInfoViewModel mDriverInfo) {
		this.mDriverInfo = mDriverInfo;
	}
}
