package kr.moonlightdriver.user.view.callDriver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.skp.Tmap.TMapAddressInfo;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.BusProvider;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.ResultCallback;
import kr.moonlightdriver.user.common.TmapManager;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.view.main.MainActivity;
import kr.moonlightdriver.user.viewholder.StopByListViewHolder;
import kr.moonlightdriver.user.viewmodel.callDriver.StopByViewModel;
import kr.moonlightdriver.user.viewmodel.common.ActivityResultEvent;
import kr.moonlightdriver.user.viewmodel.common.LocationData;

public class CallDriverFragment extends Fragment implements OnMapReadyCallback {
	private static final String TAG = "CallDriverFragment";
	private final float DEFAULT_ZOOM  = 16f;

	private TextView mTextViewDepartureFrom;
	private TextView mTextViewArriveAt;
	private LinearLayout mResultMapLayout;
	private Button mBtnCallDriver;
	private Button mBtnAddStopBy;
	private ListView mListViewStopByList;

	private LinearLayout mStopByWrapperLayout;

	private GoogleMap mGoogleMap;
	private Marker mCurrentPositionMarker;

	private MainActivity mMainActivity;
	private LatLng mCurrentLocation;

	private LocationData mDepartureFrom;
	private LocationData mArriveAt;
	private ArrayList<LocationData> mStopByList;
	private ArrayList<StopByViewModel> mStopByViewModelList;

	private StopByListAdapter mStopByListAdapter;

	private View mView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			try {
			mView = inflater.inflate(R.layout.fragment_call_driver, container, false);

			mMainActivity = (MainActivity) getActivity();
			mDepartureFrom = new LocationData();
			mArriveAt = new LocationData();
			mStopByList = new ArrayList<>();

			mListViewStopByList = (ListView) mView.findViewById(R.id.stopby_list);
			mTextViewDepartureFrom = (TextView) mView.findViewById(R.id.textview_depart_from);
			mTextViewArriveAt = (TextView) mView.findViewById(R.id.textview_arrive_at);
			mResultMapLayout = (LinearLayout) mView.findViewById(R.id.map_layout);
			mResultMapLayout.setVisibility(View.VISIBLE);
			mBtnAddStopBy = (Button) mView.findViewById(R.id.btnAddStopBy);
			mBtnCallDriver = (Button) mView.findViewById(R.id.btnCallDriver);

			mStopByWrapperLayout = (LinearLayout) mView.findViewById(R.id.stopby_wrapper_layout);

			mStopByViewModelList = new ArrayList<>();
			mStopByListAdapter = new StopByListAdapter(mMainActivity, R.layout.row_stopby_list, mStopByViewModelList);
			mListViewStopByList.setAdapter(mStopByListAdapter);

			mCurrentLocation = mMainActivity.mCurrentLocation;

			initEventListener();

			setUpMapIfNeeded();

			setDepartureLocation();
		} catch(Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void initEventListener() {
		mTextViewDepartureFrom.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSearchLocation("departure");
			}
		});

		mTextViewArriveAt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSearchLocation("arrive");
			}
		});

		mBtnCallDriver.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

			}
		});

		mBtnAddStopBy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					if(mStopByViewModelList.size() >= 3) {
						return;
					}

					mStopByViewModelList.add(new StopByViewModel("경유"));

					mStopByListAdapter.notifyDataSetChanged();

					setStopByButton();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void setStopByButton() {
		try {
			if(mStopByViewModelList.size() >= 3) {
				mBtnAddStopBy.setEnabled(false);
			} else {
				mBtnAddStopBy.setEnabled(true);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void showSearchLocation(String locationType) {
		try {
			SearchLocationFragment searchLocationFragment = new SearchLocationFragment();

			Bundle searchParams = new Bundle();
			searchParams.putString("location_type", locationType);

			searchLocationFragment.setArguments(searchParams);
			searchLocationFragment.setTargetFragment(this, Configuration.REQ_SEARCH_LOCATION);

			mMainActivity.addFragment(searchLocationFragment);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void setDepartureLocation() {
		try {
			TmapManager tmapManager = new TmapManager();
			tmapManager.convertGpsToAddress(mCurrentLocation, new ResultCallback() {
				@Override
				public void resultCallback(HashMap<String, Object> params) {
					try {
						TMapAddressInfo addressInfo = (TMapAddressInfo) params.get("addressInfo");
						if(addressInfo != null) {
							mTextViewDepartureFrom.setText(addressInfo.strBuildingName);
							mDepartureFrom.setmLocationName(addressInfo.strBuildingName);
							mDepartureFrom.setmAddress(addressInfo.strFullAddress);
							mDepartureFrom.setmLocation(mCurrentLocation);
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.result_map);
				mapFragment.getMapAsync(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void changeMyLocation() {
		try {
			Utils.log("error", TAG, "===== changeMyLocation() =====");
			if(mGoogleMap != null) {
				mCurrentLocation = mMainActivity.mCurrentLocation;

				if(mCurrentPositionMarker != null) {
					mCurrentPositionMarker.setPosition(mCurrentLocation);
				}

				mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(mCurrentLocation));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class StopByListAdapter extends ArrayAdapter<StopByViewModel> {
		private ArrayList<StopByViewModel> mItems;
		private int mLayoutID;
		private StopByListViewHolder mViewHolder;

		public StopByListAdapter(Context _context, int _layoutID, ArrayList<StopByViewModel> _items) {
			super(_context, _layoutID, _items);

			this.mItems = _items;
			this.mLayoutID = _layoutID;
		}

		@Override
		public View getView(int _position, View _convertView, ViewGroup _parent) {
			View view = _convertView;

			try {
				if(view == null) {
					LayoutInflater inflater = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					view = inflater.inflate(mLayoutID, null);

					mViewHolder = new StopByListViewHolder();

					mViewHolder.mTitle = (TextView) view.findViewById(R.id.textview_stopby_title);
					mViewHolder.mStopByLocation = (TextView) view.findViewById(R.id.textview_stopby_location);
					mViewHolder.mDeleteStopBy = (Button) view.findViewById(R.id.btnDeleteStopBy);

					view.setTag(mViewHolder);
				} else {
					mViewHolder = (StopByListViewHolder) view.getTag();
				}

				StopByViewModel stopByViewModel = mItems.get(_position);
				String title = stopByViewModel.getmTitle() + "" + (_position + 1);

				if(stopByViewModel.getmStopBy() != null && stopByViewModel.getmStopBy().getmLocationName() != null && !stopByViewModel.getmStopBy().getmLocationName().isEmpty()) {
					mViewHolder.mStopByLocation.setText(stopByViewModel.getmStopBy().getmLocationName());
				} else if(stopByViewModel.getmStopBy() != null && stopByViewModel.getmStopBy().getmAddress() != null && !stopByViewModel.getmStopBy().getmAddress().isEmpty()) {
					mViewHolder.mStopByLocation.setText(stopByViewModel.getmStopBy().getmAddress());
				} else {
					mViewHolder.mStopByLocation.setText("");
				}

				mViewHolder.mTitle.setText(title);
				mViewHolder.mStopByLocation.setTag(_position);
				mViewHolder.mStopByLocation.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						try {
							int selectedIndex = (int)view.getTag();

							showSearchLocation("stopby_" + selectedIndex);
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				});

				mViewHolder.mDeleteStopBy.setTag(_position);
				mViewHolder.mDeleteStopBy.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						try {
							int selectedIndex = (int)view.getTag();

							mStopByViewModelList.remove(selectedIndex);
							mStopByListAdapter.notifyDataSetChanged();

							setStopByButton();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} catch(Exception e) {
				e.printStackTrace();
			}

			return view;
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		try {
			mGoogleMap = googleMap;

			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);

			mCurrentPositionMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLocation));
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, DEFAULT_ZOOM));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			if(resultCode == Activity.RESULT_OK) {
				if(requestCode == Configuration.REQ_SEARCH_LOCATION) {
					String locationType = data.getStringExtra("locationType");
					String locationName = data.getStringExtra("locationName");
					String address = data.getStringExtra("address");
					double lat = data.getDoubleExtra("lat", 0);
					double lng = data.getDoubleExtra("lng", 0);

					if(locationType.equals("departure")) {
						mDepartureFrom.setmLocation(new LatLng(lat, lng));
						mDepartureFrom.setmLocationName(locationName);
						mDepartureFrom.setmAddress(address);

						mTextViewDepartureFrom.setText(mDepartureFrom.getmLocationName());
					} else if(locationType.equals("arrive")) {
						mArriveAt.setmLocation(new LatLng(lat, lng));
						mArriveAt.setmLocationName(locationName);
						mArriveAt.setmAddress(address);

						mTextViewArriveAt.setText(mArriveAt.getmLocationName());
					} else if(locationType.startsWith("stopby_")) {
						String[] splitLocationType = locationType.split("_");
						int index = Integer.parseInt(splitLocationType[1]);
						LocationData stopByLocation = new LocationData();
						stopByLocation.setmLocation(new LatLng(lat, lng));
						stopByLocation.setmLocationName(locationName);
						stopByLocation.setmAddress(address);

						StopByViewModel stopByViewModel = mStopByViewModelList.get(index);
						stopByViewModel.setmStopBy(stopByLocation);

						mStopByViewModelList.set(index, stopByViewModel);

						mStopByListAdapter.notifyDataSetChanged();
					}
				} else if(requestCode == Configuration.POSITION_CHANGED) {
					changeMyLocation();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void onActivityResult(ActivityResultEvent _activityResultEvent) {
		try {
			onActivityResult(_activityResultEvent.getRequestCode(), _activityResultEvent.getResultCode(), _activityResultEvent.getData());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDestroyView() {
		BusProvider.getInstance().unregister(this);
		super.onDestroyView();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		BusProvider.getInstance().register(this);
	}
}
