package kr.moonlightdriver.user.view.main;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.model.LatLng;
import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;
import com.loopj.android.http.RequestParams;
import com.skp.Tmap.TMapView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import kr.moonlightdriver.user.R;
import kr.moonlightdriver.user.common.BusProvider;
import kr.moonlightdriver.user.common.CommonDialog;
import kr.moonlightdriver.user.common.Configuration;
import kr.moonlightdriver.user.common.SendLocationService;
import kr.moonlightdriver.user.common.Utils;
import kr.moonlightdriver.user.common.network.NetAPI;
import kr.moonlightdriver.user.common.network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.user.common.network.NetApi.NoticeListAck;
import kr.moonlightdriver.user.common.network.NetApi.UserLoginAck;
import kr.moonlightdriver.user.common.network.NetClient;
import kr.moonlightdriver.user.common.network.NetResponse;
import kr.moonlightdriver.user.common.network.NetResponseCallback;
import kr.moonlightdriver.user.view.callDriver.CallDriverFragment;
import kr.moonlightdriver.user.view.chauffeurCompany.ChauffeurCompanyListFragment;
import kr.moonlightdriver.user.view.crackdown.CrackdownFragment;
import kr.moonlightdriver.user.view.settings.SettingsFragment;
import kr.moonlightdriver.user.viewmodel.common.ActivityResultEvent;
import kr.moonlightdriver.user.viewmodel.common.NoticeData;
import kr.moonlightdriver.user.viewmodel.common.ResultData;
import kr.moonlightdriver.user.viewmodel.common.UserAdditionalViewModel;
import kr.moonlightdriver.user.viewmodel.common.UserViewModel;

import static kr.moonlightdriver.user.common.Utils.getGCMPreferences;
import static kr.moonlightdriver.user.common.Utils.log;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
	private static final String TAG = "MainActivity";

	public LatLng mCurrentLocation;
	private LatLng mLastUpdateLocation;

	private ImageButton mBtnPhoneList;
	private ImageButton mBtnCallDriver;
	private ImageButton mBtnDrunkenDrivingCheck;
	private ImageButton mBtnSettings;
	private LinearLayout mTabMenuLayout;
	private RelativeLayout mPolicyServiceLayout;
	private ImageButton mBtnPolicyOk;
	private ImageButton mBtnCheckPolicyAll;
	private ImageButton mBtnCheckPolicyService;
	private ImageButton mBtnCheckPolicyPrivacy;
	private ImageButton mBtnCheckPolicyLocation;
	private ImageButton mBtnPopupConfirmClose;

	private LinearLayout mPopupCommonConfirmLayout;
	private TextView mPopupConfirmTitle;
	private TextView mPopupConfirmContent;
	private ImageButton mBtnConfirmNo;
	private ImageButton mBtnConfirmYes;

	private ChauffeurCompanyListFragment mFragPhoneList;
	private CallDriverFragment mFragCallDriver;
	private CrackdownFragment mFragCrackdown;
	private SettingsFragment mFragSettings;

	private LinearLayout mNoticeLayout;
	private TextView mNoticeTitle;
	private TextView mNoticeContent;
	private ImageView mNoticeImage;
	private TextView mNoticeLink;
	private ImageButton mBtnNotAllowOpen;;
	private ImageButton mBtnNoticeClose;

	private RelativeLayout mPopupSplashLayout;

	// gcm
	public String mUserPhoneNumber;
	private GoogleCloudMessaging mGcm;
	public String mRegistrationId;
	public UserViewModel mUserDetail;
	public UserAdditionalViewModel mUserAdditionalData;

	private View mCurrentView;
	private Context mContext;

//	private GpsInfo mGpsInfo;

//	private boolean mNeedUpdate = false;

//	public Bitmap mCurrentPositionInnerIcon;
//	public Bitmap mCurrentPositionOuterIcon;

	public Typeface mFontSeoulNamsanL;
	public Typeface mFontSeoulNamsanM;
	public Typeface mFontSeoulNamsanB;
	public Typeface mFontSeoulNamsanJangL;
	public Typeface mFontSeoulNamsanJangM;
	public Typeface mFontSeoulNamsanVert;

	public RelativeLayout mLoadingProgress;

	public KakaoLink mKakaoLink;
	public KakaoTalkLinkMessageBuilder mKakaoTalkLinkMessageBuilder;

	public ArrayList<ResolveInfo> mCrackdownShareAppInfoList;

	private long mLastPressTime;

	private ArrayList<NoticeData> mNoticeDataList;

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		try {
//			SVUtil.log("error", TAG, "onNewIntent()");

			Bundle extras = intent.getExtras();
			if(extras != null) {
				recvMessage(extras);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.activity_main);

			BusProvider.getInstance().register(this);

			mContext = this;
			mCurrentLocation = Configuration.SEOUL_CITY_HALL;

			TMapView tmapview = new TMapView(this);
			tmapview.setSKPMapApiKey(Configuration.TMAP_API_KEY);

			mKakaoLink = KakaoLink.getKakaoLink(this);
			mKakaoTalkLinkMessageBuilder = mKakaoLink.createKakaoTalkLinkMessageBuilder();

			mLoadingProgress = (RelativeLayout) findViewById(R.id.loading_progress);
			hideLoadingAnim();

//			initGpsInfo();
//			startLocationService();

			mCrackdownShareAppInfoList = Utils.getShareIntents(getPackageManager(), "com.android.mms", "com.kakao.talk");

			mPopupSplashLayout = (RelativeLayout) findViewById(R.id.splash_layout);
			mNoticeLayout = (LinearLayout) findViewById(R.id.notice_layout);
			mNoticeTitle = (TextView) findViewById(R.id.notice_title);
			mNoticeContent = (TextView) findViewById(R.id.notice_content);
			mNoticeImage = (ImageView) findViewById(R.id.notice_image);
			mNoticeImage.setVisibility(View.GONE);
			mNoticeLink = (TextView) findViewById(R.id.notice_link);
			mNoticeLink.setVisibility(View.GONE);
			mBtnNotAllowOpen = (ImageButton) findViewById(R.id.btn_not_allow_open);
			mBtnNoticeClose = (ImageButton) findViewById(R.id.btn_notice_close);

			mPopupCommonConfirmLayout = (LinearLayout) findViewById(R.id.popup_common_confirm_layout);
			mPopupCommonConfirmLayout.setVisibility(View.GONE);
			mTabMenuLayout = (LinearLayout) findViewById(R.id.tab_menu);
			mBtnPhoneList = (ImageButton) findViewById(R.id.tabPhoneList);
			mBtnCallDriver = (ImageButton) findViewById(R.id.tabCallDriver);
			mBtnDrunkenDrivingCheck = (ImageButton) findViewById(R.id.tabDrunkenDrivingCheck);
			mBtnSettings = (ImageButton) findViewById(R.id.tabSettings);
			mPolicyServiceLayout = (RelativeLayout) findViewById(R.id.policy_service_layout);
			mPolicyServiceLayout.setVisibility(View.GONE);
			mBtnPolicyOk = (ImageButton) findViewById(R.id.btn_policy_ok);
			mBtnCheckPolicyAll = (ImageButton) findViewById(R.id.btn_check_policy_all);
			mBtnCheckPolicyService = (ImageButton) findViewById(R.id.btn_check_policy_service);
			mBtnCheckPolicyPrivacy = (ImageButton) findViewById(R.id.btn_check_policy_privacy);
			mBtnCheckPolicyLocation = (ImageButton) findViewById(R.id.btn_check_policy_location);
			mBtnPopupConfirmClose = (ImageButton) findViewById(R.id.btn_popup_confirm_close);
			mBtnConfirmNo = (ImageButton) findViewById(R.id.btn_confirm_no);
			mBtnConfirmYes = (ImageButton) findViewById(R.id.btn_confirm_yes);
			mPopupConfirmTitle = (TextView) findViewById(R.id.popup_confirm_title);
			mPopupConfirmContent = (TextView) findViewById(R.id.popup_confirm_content);

			mBtnPhoneList.setOnClickListener(this);
			mBtnCallDriver.setOnClickListener(this);
			mBtnDrunkenDrivingCheck.setOnClickListener(this);
			mBtnSettings.setOnClickListener(this);
			mBtnPolicyOk.setOnClickListener(this);
			mBtnCheckPolicyAll.setOnClickListener(this);
			mBtnCheckPolicyService.setOnClickListener(this);
			mBtnCheckPolicyPrivacy.setOnClickListener(this);
			mBtnCheckPolicyLocation.setOnClickListener(this);
			mBtnPopupConfirmClose.setOnClickListener(this);
			mBtnConfirmYes.setOnClickListener(this);
			mBtnConfirmNo.setOnClickListener(this);

			TextView policyServiceText = (TextView) findViewById(R.id.policy_service_text);
			TextView policyPrivacyText = (TextView) findViewById(R.id.policy_privacy_text);
			TextView policyLocationText = (TextView) findViewById(R.id.policy_location_text);

			setTabMenuVisibility(View.GONE);

			mFragPhoneList = new ChauffeurCompanyListFragment();
			mFragCallDriver = new CallDriverFragment();
			mFragCrackdown = new CrackdownFragment();
			mFragSettings = new SettingsFragment();

			mCurrentView = mBtnCallDriver;
			mLastPressTime = 0;

			initFonts();

			Utils.initRegion();

//			getCurrentPositionIcon();

			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

			Utils.setFontToView(mFontSeoulNamsanJangM, policyServiceText, policyPrivacyText, policyLocationText, mPopupConfirmContent);
			Utils.setFontToView(mFontSeoulNamsanVert, mPopupConfirmTitle);

			checkNetworkStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getUserPhoneNumber() {
		try {
			if(checkPhoneNumberPermission()) {
				TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				mUserPhoneNumber = tMgr.getLine1Number();

//				mUserPhoneNumber = "+8201011110000";

				if(mUserPhoneNumber == null) {
					CommonDialog.showDialogWithListener(mContext, "전화번호 조회에 실패했습니다.\n전화번호가 없으면 사용이 불가능합니다.", "확인", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Utils.finishActivity((Activity) mContext);
						}
					});
				} else {
					checkPlayServices();
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void initFonts() {
		try {
			mFontSeoulNamsanM = Typeface.createFromAsset(getAssets(), "fonts/SeoulNamsanM.otf");
			mFontSeoulNamsanL = Typeface.createFromAsset(getAssets(), "fonts/SeoulNamsanL.otf");
			mFontSeoulNamsanB = Typeface.createFromAsset(getAssets(), "fonts/SeoulNamsanB.otf");
			mFontSeoulNamsanVert = Typeface.createFromAsset(getAssets(), "fonts/SeoulNamsanvert.otf");
			mFontSeoulNamsanJangL = Typeface.createFromAsset(getAssets(), "fonts/SeoulNamsanJangL.otf");
			mFontSeoulNamsanJangM = Typeface.createFromAsset(getAssets(), "fonts/SeoulNamsanJangM.otf");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void removeSplash() {
		try {
			mPopupSplashLayout.animate().setDuration(500).alpha(0f).setListener(new Animator.AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animator) {

				}

				@Override
				public void onAnimationEnd(Animator animator) {
					try {
						mPopupSplashLayout.setVisibility(View.GONE);

						Bundle extras = getIntent().getExtras();
						if(extras != null) {
							recvMessage(extras);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onAnimationCancel(Animator animator) {

				}

				@Override
				public void onAnimationRepeat(Animator animator) {

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void onActivityResult(ActivityResultEvent _activityResultEvent) {
		try {
			onActivityResult(_activityResultEvent.getRequestCode(), _activityResultEvent.getResultCode(), _activityResultEvent.getData());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	private void getCurrentPositionIcon() {
//		try {
//			int width;
//			int height;
//			BitmapDrawable bitmapDrawable;
//
//			if(mCurrentPositionInnerIcon == null) {
//				width = Utils.dpFromPx(mContext, 123);
//				height = Utils.dpFromPx(mContext, 123);
//				bitmapDrawable = ((BitmapDrawable) getResources().getDrawable(R.drawable.ic_current_location_inner));
//				if(bitmapDrawable != null) {
//					mCurrentPositionInnerIcon = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), width, height, false);
//				}
//			}
//
//			if(mCurrentPositionOuterIcon == null) {
//				width = Utils.dpFromPx(mContext, 123);
//				height = Utils.dpFromPx(mContext, 169);
//				bitmapDrawable = ((BitmapDrawable) getResources().getDrawable(R.drawable.ic_current_location_outer));
//				if(bitmapDrawable != null) {
//					mCurrentPositionOuterIcon = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), width, height, false);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void checkApplicationVersion() {
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String version = pInfo.versionName;

			NetClient.send(mContext, Configuration.URL_CHECK_VERSION + "?version=" + version, "GET", null, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null) {
							NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == 100) {
								userLogin();
							} else if(result.getmCode() == 236){
								new AlertDialog.Builder(mContext)
									.setIcon(android.R.drawable.ic_dialog_info)
									.setCancelable(false)
									.setTitle("업데이트")
									.setMessage("앱을 업데이트 하시겠습니까?")
									.setPositiveButton("업데이트", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
											marketLaunch.setData(Uri.parse(Configuration.APP_DOWNLOAD_URL));
											startActivity(marketLaunch);

											Utils.finishActivity(MainActivity.this);
										}
									})
									.setNegativeButton("종료", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialogInterface, int i) {
											Utils.finishActivity(MainActivity.this);
										}
									})
									.show();
							} else {
								CommonDialog.showDialogWithListener(mContext, "버전 조회에 실패했습니다.\n사용 불가능한 버전입니다.", "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Utils.finishActivity((Activity) mContext);
									}
								});
							}
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void showPolicyPopup() {
		try {
			final SharedPreferences prefs = getGCMPreferences(mContext);
			boolean isFirstLaunch = prefs.getBoolean(Configuration.PROPERTY_FIRST_LAUNCH, true);

			if(!isFirstLaunch) {
				registerGcmId();
				return;
			}

			mPolicyServiceLayout.setVisibility(View.VISIBLE);
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public void registerGcmId() {
		try {
			// If this check succeeds, proceed with normal processing.
			// Otherwise, prompt user to get valid Play Services APK.
			mGcm = GoogleCloudMessaging.getInstance(this);
			mRegistrationId = getRegistrationId(mContext);

			registerInBackground();

//			if (mRegistrationId.isEmpty()) {
//				registerInBackground();
//			} else {
//				checkApplicationVersion();
//			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void registerInBackground() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... voids) {
				try {
					if (mGcm == null) {
						mGcm = GoogleCloudMessaging.getInstance(mContext);
					}

					mRegistrationId = mGcm.register(Configuration.SENDER_ID);

					storeRegistrationId(mRegistrationId);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(Void _void) {
				super.onPostExecute(_void);

				checkApplicationVersion();
			}
		}.execute();
	}

	public void userLogin() {
		try {
			RequestParams params = new RequestParams();
			params.add("phone", mUserPhoneNumber);
			params.add("macAddr", "");
			params.add("gcmId", mRegistrationId);
			params.add("device_uuid", Utils.getDeviceUUID(mContext));
			params.add("lat", "" + mCurrentLocation.latitude);
			params.add("lng", "" + mCurrentLocation.longitude);
			params.add("email", "");
			params.add("name", "");

			NetClient.send(mContext, Configuration.URL_AUTO_REGISTER_USER, "POST", params, new UserLoginAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null) {
							UserLoginAck retNetApi = (UserLoginAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == 100) {
								mUserDetail = retNetApi.getmUserDetailData();
								mUserAdditionalData = retNetApi.getmUserAdditionalData();

								saveUserId(mUserDetail.getmUserId());

								finishInit();
							} else {
								CommonDialog.showDialogWithListener(mContext, "사용자 인증에 실패했습니다.\n잠시후 다시 시도해주세요.", "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Utils.finishActivity((Activity) mContext);
									}
								});
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveUserId(String _userId) {
		try {
			final SharedPreferences prefs = getGCMPreferences(mContext);
			prefs.edit().putString(Configuration.PROPERTY_USER_ID, _userId).commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void finishInit() {
		try {
			replaceFragment(mFragCrackdown);
			getNotice();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getNotice() {
		try {
			mNoticeDataList = new ArrayList<>();

			NetClient.send(mContext, Configuration.URL_NOTICE_LIST, "GET", null, new NoticeListAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null) {
							NoticeListAck retNetApi = (NoticeListAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == 100) {
								mNoticeDataList = retNetApi.getmNoticeDataList();

								showNoticePopup();
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initNoticePopup() {
		try {
			mNoticeTitle.setText("");
			mNoticeContent.setText("");
			mNoticeImage.setImageURI(null);
			mNoticeImage.setVisibility(View.GONE);
			mNoticeLink.setOnClickListener(null);
			mNoticeLink.setVisibility(View.GONE);
			mBtnNotAllowOpen.setSelected(false);
			mBtnNotAllowOpen.setOnClickListener(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showNoticePopup() {
		try {
			if(mNoticeDataList.size() <= 0) {
				return;
			}

			final NoticeData noticeData = mNoticeDataList.get(0);

			if(!checkAllowNotice(noticeData.getmUserNoticeId())) {
				mNoticeDataList.remove(0);
				showNoticePopup();
				return;
			}

			initNoticePopup();

			mNoticeTitle.setText(noticeData.getmTitle());
			mNoticeContent.setText(noticeData.getmContents());

			if(!noticeData.getmImagePath().isEmpty()) {
				new Utils.DownloadImageTask(mNoticeImage).execute(Configuration.IMAGE_SERVER_HOST + noticeData.getmImagePath());
			}

			if(!noticeData.getmLinkUrl().isEmpty()) {
				mNoticeLink.setVisibility(View.VISIBLE);
				mNoticeLink.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						try {
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setData(Uri.parse(noticeData.getmLinkUrl()));
							startActivity(intent);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}

			mBtnNotAllowOpen.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						mBtnNotAllowOpen.setSelected(!mBtnNotAllowOpen.isSelected());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnNoticeClose.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						closeNoticePopup(mBtnNotAllowOpen.isSelected(), noticeData.getmUserNoticeId());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mNoticeLayout.setVisibility(View.VISIBLE);

			mNoticeDataList.remove(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void closeNoticePopup(boolean _isNotAllowNotice, String _userNoticeId) {
		try {
			if(_isNotAllowNotice) {
				saveNotAllowNotice(_userNoticeId);
			}

			mNoticeLayout.animate().setDuration(500).alpha(0f).setListener(new Animator.AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animator) {

				}

				@Override
				public void onAnimationEnd(Animator animator) {
					try {
						mNoticeLayout.setVisibility(View.GONE);
						mNoticeLayout.setAlpha(1f);
						showNoticePopup();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onAnimationCancel(Animator animator) {

				}

				@Override
				public void onAnimationRepeat(Animator animator) {

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveNotAllowNotice(String _userNoticeId) {
		try {
			final SharedPreferences prefs = getGCMPreferences(mContext);
			prefs.edit().putLong(_userNoticeId, System.currentTimeMillis()).commit();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkAllowNotice(String _userNoticeId) {
		try {
			final SharedPreferences prefs = getGCMPreferences(mContext);
			long noShowNoticeTime = prefs.getLong(_userNoticeId, 0);
			if((System.currentTimeMillis() - noShowNoticeTime) < (7 * 24 * 60 * 60 * 1000)) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	private void storeRegistrationId(String _regId) {
		try {
			final SharedPreferences prefs = getGCMPreferences(mContext);
			int appVersion = getAppVersion(mContext);

			SharedPreferences.Editor editor = prefs.edit();
			editor.putString(Configuration.PROPERTY_REG_ID, _regId);
			editor.putInt(Configuration.PROPERTY_APP_VERSION, appVersion);
			editor.apply();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkPlayServices() {
		try {
			GoogleApiAvailability api = GoogleApiAvailability.getInstance();
			int resultCode = api.isGooglePlayServicesAvailable(this);
			if (resultCode == ConnectionResult.SUCCESS) {
				onActivityResult(Configuration.REQUEST_GOOGLE_PLAY_SERVICES, Activity.RESULT_OK, null);
			} else if(api.isUserResolvableError(resultCode) && api.showErrorDialogFragment(this, resultCode, Configuration.REQUEST_GOOGLE_PLAY_SERVICES)) {

			} else {
				CommonDialog.showDialogWithListener(mContext, "지원하지 않는 단말기입니다.", "확인", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Utils.finishActivity((Activity) mContext);
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkNetworkStatus() {
		try {
			int networkStatus = Utils.GetLocationServiceStatus(mContext);

			if (networkStatus == 0) {
				try {
//					mContext.sendBroadcast(new Intent("com.skt.intent.action.GPS_TURN_ON"));
					PopUpLocationSettingDialog(2);
				} catch (Exception e) {
					PopUpLocationSettingDialog(0);
				}
			} else if (networkStatus == 2) {
				PopUpLocationSettingDialog(2);
			} else if (networkStatus == 1) {
				if (!Utils.GetWifiOnStatus(mContext)) {
					PopUpWifiSettingDialog();
				}
			}

			startLocationService();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void PopUpLocationSettingDialog(int status) {
		try {
			String msg = "";

			if (status == 0) {
				msg = "달빛기사에서 내 위치 정보를 사용하려면, 단말기의 설정에서 '위치 서비스' 사용을 허용해주세요.";
			} else if (status == 2) {
				msg = "위치서비스에서 무선 네트워크 사용을 허용해주세요. 위치를 빠르게 찾을 수 있습니다.";
			}

			CommonDialog.showDialogWithListener(mContext, "위치 서비스", msg, "설정하기", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					((Activity) mContext).startActivityForResult(viewIntent, Configuration.GPS_SETTING_REQ_CODE);
				}
			}, "취소", null);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void PopUpWifiSettingDialog() {
		try {
			CommonDialog.showDialogWithListener(mContext, "Wi-Fi", "Wi-Fi를 켜면 위치 정확도를 높일 수 있습니다.", "설정하기", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent viewIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
					((Activity) mContext).startActivityForResult(viewIntent, Configuration.WIFI_SETTING_REQ_CODE);
				}
			}, "취소", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getRegistrationId(Context _context) {
		try {
			final SharedPreferences prefs = getGCMPreferences(_context);
			String registrationId = prefs.getString(Configuration.PROPERTY_REG_ID, "");

			if (registrationId.isEmpty()) {
				return "";
			}

			int registeredVersion = prefs.getInt(Configuration.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
			int currentVersion = getAppVersion(_context);
			if (registeredVersion != currentVersion) {
				return "";
			}

			return registrationId;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

			return packageInfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			// should never happen
			e.printStackTrace();
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	public void setTabMenuVisibility(int visibility) {
		try {
			if(mTabMenuLayout != null) {
				// TODO : 고객용 1차 버전에서는 탭이 보이면 안된다. 2차 버전에서 탭 오픈
				mTabMenuLayout.setVisibility(visibility);
//				mTabMenuLayout.setVisibility(View.GONE);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

//	private void initGpsInfo() {
//		try {
//			if(mGpsInfo != null) {
//				if(mGpsInfo.isConnected()) {
//					mGpsInfo.stopLocationUpdates();
//				}
//				mGpsInfo = null;
//			}
//
//			mGpsInfo = new GpsInfo(MainActivity.this, new GpsLocationListener());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	public void startLocationService() {
		try {
			Utils.log("error", TAG, "startLocationService()");

			if(checkLocationPermission()) {
				Intent serviceIntent = new Intent();
				serviceIntent.putExtra("start_from", "main");
				serviceIntent.setClass(this, SendLocationService.class);

				stopService(serviceIntent);

				startService(serviceIntent);

				getUserPhoneNumber();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showConfirmPopup(String _title, String _message, View.OnClickListener _clickListener) {
		try {
			if(_title == null || _title.isEmpty()) {
				_title = "알림";
			}

			mPopupConfirmTitle.setText(_title);
			mPopupConfirmContent.setText(_message);
			mBtnConfirmYes.setOnClickListener(_clickListener);

			mPopupCommonConfirmLayout.setVisibility(View.VISIBLE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View _view) {
		try {
			if(_view == mBtnPhoneList) {
				if(mCurrentView == _view) {
					return;
				}

				mCurrentView = _view;
				replaceFragment(mFragPhoneList);
			} else if(_view == mBtnCallDriver) {
				if(mCurrentView == _view) {
					return;
				}

				mCurrentView = _view;
				replaceFragment(mFragCallDriver);
			} else if(_view == mBtnDrunkenDrivingCheck) {
				if(mCurrentView == _view) {
					return;
				}

				mCurrentView = _view;
				replaceFragment(mFragCrackdown);
			} else if(_view == mBtnSettings) {
				if(mCurrentView == _view) {
					return;
				}

				mCurrentView = _view;
				replaceFragment(mFragSettings);
			} else if(_view == mBtnPolicyOk) {
				if(!(mBtnCheckPolicyLocation.isSelected() && mBtnCheckPolicyPrivacy.isSelected() && mBtnCheckPolicyService.isSelected())) {
					CommonDialog.showSimpleDialog(mContext, "달빛기사 이용약관 및 개인정보 취급 방침, 위치기반 서비스 이용 약관 모두 동의해 주세요. ");
					return;
				}

				final SharedPreferences prefs = getGCMPreferences(mContext);
				prefs.edit().putBoolean(Configuration.PROPERTY_FIRST_LAUNCH, false).apply();
				mPolicyServiceLayout.setVisibility(View.GONE);

				registerGcmId();
			} else if(_view == mBtnCheckPolicyAll) {
				mBtnCheckPolicyAll.setSelected(!mBtnCheckPolicyAll.isSelected());

				if(mBtnCheckPolicyAll.isSelected()) {
					mBtnCheckPolicyService.setSelected(true);
					mBtnCheckPolicyPrivacy.setSelected(true);
					mBtnCheckPolicyLocation.setSelected(true);
				} else {
					mBtnCheckPolicyService.setSelected(false);
					mBtnCheckPolicyPrivacy.setSelected(false);
					mBtnCheckPolicyLocation.setSelected(false);
				}
			} else if(_view == mBtnCheckPolicyService) {
				mBtnCheckPolicyService.setSelected(!mBtnCheckPolicyService.isSelected());

				checkPolicyAll();
			} else if(_view == mBtnCheckPolicyPrivacy) {
				mBtnCheckPolicyPrivacy.setSelected(!mBtnCheckPolicyPrivacy.isSelected());

				checkPolicyAll();
			} else if(_view == mBtnCheckPolicyLocation) {
				mBtnCheckPolicyLocation.setSelected(!mBtnCheckPolicyLocation.isSelected());

				checkPolicyAll();
			} else if(_view == mBtnPopupConfirmClose || _view == mBtnConfirmNo) {
				closeConfirmPopup();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeConfirmPopup() {
		try {
			mPopupCommonConfirmLayout.setVisibility(View.GONE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkPolicyAll() {
		try {
			if(!(mBtnCheckPolicyLocation.isSelected() && mBtnCheckPolicyPrivacy.isSelected() && mBtnCheckPolicyService.isSelected())) {
				mBtnCheckPolicyAll.setSelected(false);
			} else if(mBtnCheckPolicyLocation.isSelected() && mBtnCheckPolicyPrivacy.isSelected() && mBtnCheckPolicyService.isSelected()) {
				mBtnCheckPolicyAll.setSelected(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void replaceFragment(Fragment _targetFragment) {
		try {
			getSupportFragmentManager().beginTransaction().replace(R.id.rootFrame, _targetFragment).addToBackStack(null).commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addFragment(Fragment _targetFragment) {
		try {
			getSupportFragmentManager().beginTransaction().add(R.id.rootFrame, _targetFragment).addToBackStack(null).commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeFragment(Fragment _targetFragment) {
		try {
			getSupportFragmentManager().beginTransaction().remove(_targetFragment).commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void makePhoneCall() {
		try {
			if(checkPhoneCallPermission(Configuration.CALL_PHONE_REQ_CODE)) {
				String uri = "tel:" + Configuration.HEADQUARTER_NUMBER;

				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse(uri));

				startActivity(intent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean checkPhoneCallPermission(int _reqCode) {
		boolean isPermissionGranted = true;

		try {
			if(Build.VERSION.SDK_INT >= 23) {
				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions((Activity) mContext, new String[] { Manifest.permission.CALL_PHONE }, _reqCode);
					isPermissionGranted = false;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			isPermissionGranted = false;
		}

		return isPermissionGranted;
	}

	private boolean checkPhoneNumberPermission() {
		boolean isPermissionGranted = true;

		try {
			if(Build.VERSION.SDK_INT >= 23) {
				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions((Activity) mContext, new String[] { Manifest.permission.READ_PHONE_STATE }, Configuration.PHONE_NUMBER_REQ_CODE);
					isPermissionGranted = false;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			isPermissionGranted = false;
		}

		return isPermissionGranted;
	}

	private boolean checkLocationPermission() {
		boolean isPermissionGranted = true;

		try {
			if(Build.VERSION.SDK_INT >= 23) {
				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions((Activity) mContext, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION  }, Configuration.ACCESS_LOCATION_REQ_CODE);
					isPermissionGranted = false;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			isPermissionGranted = false;
		}

		return isPermissionGranted;
	}

	public void showLoadingAnim() {
		try {
			mLoadingProgress.setVisibility(View.VISIBLE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void hideLoadingAnim() {
		try {
			mLoadingProgress.setVisibility(View.GONE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	private void updateDriverLocation(final LatLng _newLocation) {
//		try {
//			if(mUserDetail == null) {
//				return;
//			}
//
//			RequestParams params = new RequestParams();
//			params.add("userId", mUserDetail.getmUserId());
//			params.add("lat", _newLocation.latitude + "");
//			params.add("lng", _newLocation.longitude + "");
//
//			NetClient.send(mContext, Configuration.URL_UPDATE_USER_LOCATION, "POST", params, new UpdateUserLocationAck(), new NetResponseCallback(new NetResponse() {
//				@Override
//				public void onResponse(NetAPI _netAPI) {
//					try {
//						if (_netAPI != null) {
//							UpdateUserLocationAck retNetApi = (UpdateUserLocationAck) _netAPI;
//							ResultData result = retNetApi.getmResultData();
//
//							if (result.getmCode() == 100) {
//								mLastUpdateLocation = new LatLng(_newLocation.latitude, _newLocation.longitude);
//
//								if(retNetApi.getmCrackdownDistance() >= 0) {
//									showCrackdownWarningPopup(retNetApi.getmCrackdownDistance());
//								}
//							} else {
//								Utils.log("error", TAG, result.getmDetail());
//							}
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void recvMessage(Bundle _extra) {
		try {
			String messageType = _extra.getString("messageType", "");
			if(messageType.equals("showCrackdownWarning")) {
				float distance = _extra.getFloat("distance", -1);
				showCrackdownWarningPopup(distance);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showCrackdownWarningPopup(float _distance) {
		try {
			if(_distance < 0 || _distance > 1000) {
				return;
			}

			final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			long[] pattern = new long[] { 200, 1000, 200, 1000, 200, 1000 };
			String message = "";

			if(_distance <= 300) {
				message = "반경 300m 이내에 음주단속 지점이 있습니다.";
				vibrator.vibrate(pattern, 0);
			} else if(_distance <= 500) {
				message = "반경 500m 이내에 음주단속 지점이 있습니다.";
				vibrator.vibrate(pattern, -1);
			} else if(_distance <= 1000) {
				message = "반경 1Km 이내에 음주단속 지점이 있습니다.";
				vibrator.vibrate(2000);
			}

			CommonDialog.showDialogWithListener(mContext, message, "확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					vibrator.cancel();
					dialogInterface.dismiss();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	private class GpsLocationListener implements LocationListener {
//		@Override
//		public void onLocationChanged(Location location) {
//			try {
//				mCurrentLocation = new LatLng(location.getLatitude(), location.getLongitude());
//				log("error", TAG, "provider : " + location.getProvider() + ", latlng : " + mCurrentLocation);
//
//				updateDriverLocation(mCurrentLocation);
//
//				BusProvider.getInstance().post(new ActivityResultEvent(Configuration.POSITION_CHANGED, Activity.RESULT_OK, null));
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			switch (requestCode) {
				case Configuration.GPS_SETTING_REQ_CODE :
				case Configuration.WIFI_SETTING_REQ_CODE :
//					initGpsInfo();
					startLocationService();
					break;
				case Configuration.REQUEST_GOOGLE_PLAY_SERVICES :
					if (resultCode == Activity.RESULT_OK) {
//						registerGcmId();
						showPolicyPopup();
					}
					break;
				case Configuration.SELECT_PHOTO_REQ_CODE :
					BusProvider.getInstance().post(new ActivityResultEvent(Configuration.SELECT_PHOTO_RES_CODE, resultCode, data));
					break;
				case Configuration.POSITION_CHANGED :
					if(data != null) {
						double lat = data.getDoubleExtra("lat", 0);
						double lng = data.getDoubleExtra("lng", 0);

						if(lat > 0 && lng > 0) {
							mCurrentLocation = new LatLng(lat, lng);
						}
					}
					break;
//				case Configuration.WARNING_POPUP :
//					if(data != null) {
//						float distance = data.getFloatExtra("warningDistance", 0f);
//
//						if(distance > 0) {
//							showCrackdownWarningPopup(distance);
//						}
//					}
//
//					break;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		try {
//			if(mGpsInfo != null) {
//				mGpsInfo.connectGoogleApi();
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	@Override
	protected void onResume() {
		super.onResume();

		try {
//			initGpsInfo();
//			if(mGpsInfo != null && mGpsInfo.isConnected()) {
//				mGpsInfo.startLocationUpdates();
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		try {
//			initGpsInfo();
//			if(mGpsInfo != null && mGpsInfo.isConnected()) {
//				mGpsInfo.stopLocationUpdates();
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();

		try {
//			if(mGpsInfo != null) {
//				mGpsInfo.disconnectGoogleApi();
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onDestroy() {
		BusProvider.getInstance().unregister(this);

		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		try {
			BusProvider.getInstance().post(new ActivityResultEvent(Configuration.BACK_PRESSED, Activity.RESULT_OK, null));

			long pressTime = System.currentTimeMillis();

			log("error", TAG, "back stack count : " + getSupportFragmentManager().getBackStackEntryCount());

			if(pressTime - mLastPressTime <= Configuration.DOUBLE_PRESS_INTERVAL) {
				new AlertDialog.Builder(mContext)
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setCancelable(false)
						.setTitle("앱 종료")
						.setMessage("앱을 종료하시겠습니까?")
						.setPositiveButton("종료", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Utils.finishActivity(MainActivity.this);
							}
						})
						.setNegativeButton("취소", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								mLastPressTime = System.currentTimeMillis();
							}
						})
						.show();
			} else {
				if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
					super.onBackPressed();
				}

				mLastPressTime = pressTime;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		try {
			if(grantResults.length > 0) {
				for (int i = 0; i < grantResults.length; i++) {
					if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
						switch (requestCode) {
							case Configuration.CALL_PHONE_REQ_CODE:
								makePhoneCall();
								break;
							case Configuration.SECRET_CALL_PHONE_REQ_CODE:
								BusProvider.getInstance().post(new ActivityResultEvent(Configuration.SECRET_CALL_PHONE_REQ_CODE, Activity.RESULT_OK, null));
								break;
							case Configuration.PHONE_NUMBER_REQ_CODE:
								getUserPhoneNumber();
								break;
							case Configuration.ACCESS_LOCATION_REQ_CODE:
//						mGpsInfo.startLocationUpdates();
								startLocationService();
								break;
							case Configuration.CAMERA_REQ_CODE:
								BusProvider.getInstance().post(new ActivityResultEvent(Configuration.CAMERA_REQ_CODE, Activity.RESULT_OK, null));
								break;
							case Configuration.SHARE_IMAGE_REQ_CODE:
								BusProvider.getInstance().post(new ActivityResultEvent(Configuration.SHARE_IMAGE_REQ_CODE, Activity.RESULT_OK, null));
								break;
						}
					} else {
						Toast.makeText(mContext, "권한 승인이 거부 되었습니다.", Toast.LENGTH_SHORT).show();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
