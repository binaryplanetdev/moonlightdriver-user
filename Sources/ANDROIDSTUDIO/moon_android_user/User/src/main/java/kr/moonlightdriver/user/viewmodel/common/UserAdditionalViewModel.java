package kr.moonlightdriver.user.viewmodel.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2016-06-21.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAdditionalViewModel {
	@JsonProperty("userId")
	public String mUserId;

	@JsonProperty("pushYN")
	public String mPushYN;

	public UserAdditionalViewModel() {}

	public UserAdditionalViewModel(String mUserId, String mPushYN) {
		this.mUserId = mUserId;
		this.mPushYN = mPushYN;
	}

	public String getmUserId() {
		return mUserId;
	}

	public void setmUserId(String mUserId) {
		this.mUserId = mUserId;
	}

	public String getmPushYN() {
		return mPushYN;
	}

	public void setmPushYN(String mPushYN) {
		this.mPushYN = mPushYN;
	}
}
