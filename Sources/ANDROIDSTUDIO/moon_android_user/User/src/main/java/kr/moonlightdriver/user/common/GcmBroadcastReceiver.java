package kr.moonlightdriver.user.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import kr.moonlightdriver.user.view.chauffeurCompany.ChauffeurCompanyListFragment;
import kr.moonlightdriver.user.view.main.NoticePushActivity;

/**
 * Created by youngmin on 2016-10-31.
 */

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
	private static final String TAG = "GcmBroadcastReceiver";

	private String mPushMsgType;

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentService.class.getName());
			startWakefulService(context, (intent.setComponent(comp)));

			Utils.log("error", TAG, "receive : " + intent.getExtras().toString()); // 서버에서 어떻게 넘어올지 모르니 일단.. pass

			mPushMsgType = intent.getExtras().getString("type", "");

			if (mPushMsgType == null) {
				return;
			}

			switch (mPushMsgType) {
				case "register_virtual_number" :
					String virtualNumber = intent.getExtras().getString("virtual_number", "");

					Intent pushMsgIntent1 = new Intent();
					pushMsgIntent1.setAction(ChauffeurCompanyListFragment.PUSH_MSG_1);
					pushMsgIntent1.putExtra("virtualNumber", virtualNumber);

					context.sendBroadcast(pushMsgIntent1);
					break;
//				case "crackdown_warning" :
//					float distance = intent.getFloatExtra("distance", -1f);
//					int crackdownCount = intent.getIntExtra("crackdownCount", 0);
//					showCrackdownWarning(context, distance, crackdownCount);
//					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showCrackdownWarning(Context _context, float _distance, int _crackdownCount) {
		try {
			Intent intent = new Intent();
			intent.putExtra("messageType", "crackdown_warning");
			intent.putExtra("distance", _distance);
			intent.putExtra("crackdownCount", _crackdownCount);

			intent.setClass(_context, NoticePushActivity.class);
//			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

			_context.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
