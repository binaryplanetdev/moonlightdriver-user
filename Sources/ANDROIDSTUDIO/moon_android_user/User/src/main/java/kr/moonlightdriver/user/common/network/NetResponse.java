package kr.moonlightdriver.user.common.network;

/**
 * Created by youngmin on 2016-08-11.
 */
public interface NetResponse {
	void onResponse(NetAPI _netAPI);
}
