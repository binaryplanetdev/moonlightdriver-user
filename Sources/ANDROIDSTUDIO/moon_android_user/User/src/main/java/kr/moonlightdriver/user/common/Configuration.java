package kr.moonlightdriver.user.common;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

public class Configuration {
	public static final LatLng SEOUL_CITY_HALL = new LatLng(37.566221, 126.977921);

	public static final String APP_DOWNLOAD_URL = "https://play.google.com/store/apps/details?id=kr.moonlightdriver.user";
	public static final String DATABASE_NAME = "moonlightdriver.db";
	public static final int DATABASE_VERSION = 1;

	public static final long DOUBLE_PRESS_INTERVAL = 2000;

	public static final String HEADQUARTER_NUMBER = "070-7585-3100";

	public static final String PREFERENCES_NAME = "moonlightdriver_user";
	public static final String PROPERTY_DEVICE_UUID = "DEVICE_UUID";
	public static final String PROPERTY_FIRST_LAUNCH = "FIRST_LAUNCH";
	public static final String PROPERTY_REG_ID = "REG_ID";
	public static final String PROPERTY_APP_VERSION = "APP_VERSION";
	public static final String PROPERTY_USER_ID = "USER_ID";

	public static final String PATH_SHARED_IMAGE = "sharedImages";
	public static final String SHARED_TEXT = "달빛기사 - 교통정보 SNS 앱";

	public static final String SENDER_ID = "563199847799";

	// 사용자 위치 서버로 전송하는 업데이트 거리 100미터
	public static final long UPDATE_DISTANCE = 100;

	public static final int ACCESS_LOCATION_REQ_CODE = 1;
	public static final int PHONE_NUMBER_REQ_CODE = 2;
	public static final int CALL_PHONE_REQ_CODE = 3;
	public static final int GPS_SETTING_REQ_CODE = 4;
	public static final int WIFI_SETTING_REQ_CODE = 5;
	public static final int SHARE_IMAGE_REQ_CODE = 6;
	public static final int SELECT_PHOTO_REQ_CODE = 7;
	public static final int BACK_PRESSED = 8;
	public static final int POSITION_CHANGED = 9;
	public static final int REQ_SEARCH_LOCATION = 10;
	public static final int INCOMING_CALL = 11;
	public static final int CAMERA_REQ_CODE = 12;
	public static final int SECRET_CALL_PHONE_REQ_CODE = 13;
	public static final int SELECT_PHOTO_RES_CODE = 14;

	public static final int REQUEST_GOOGLE_PLAY_SERVICES = 9000;

	public static String[] REGION_LIST = new String[] { "전국콜", "서울경기",  "강원도", "충청도", "경상도", "전라도", "제주도", "대구" };
	public static JSONObject REGION;

	public static final int CRACKDOWN_REPORT_START_TIME = 19;
	public static final int CRACKDOWN_REPORT_END_TIME = 5;

	//  T-map api key
	public static final String TMAP_API_KEY	= "013703a7-d144-36c8-a049-9dafe4e4417b";

	// REAL SVR
	public static final String SERVER_HOST = "http://115.68.104.30:8888";
	public static final String IMAGE_SERVER_HOST = "http://115.68.122.108:8080";

	// DEV SVR
//	public static final String SERVER_HOST = "http://dev.sncinteractive.com:8888";
//	public static final String IMAGE_SERVER_HOST = "http://dev.sncinteractive.com:8889";

	public static final String URL_CHECK_VERSION = Configuration.SERVER_HOST + "/user/version";
	public static final String URL_AUTO_REGISTER_USER = Configuration.SERVER_HOST + "/user/register/auto";
	public static final String URL_UPDATE_USER_LOCATION = Configuration.SERVER_HOST + "/user/update/location";
	public static final String URL_UPDATE_PUSH_YN = Configuration.SERVER_HOST + "/user/update/push_yn";
	public static final String URL_DRIVER_FIND = Configuration.SERVER_HOST + "/driver/find";
	public static final String URL_CRACKDOWN_LIST = Configuration.SERVER_HOST + "/crackdown/list";
	public static final String URL_CRACKDOWN_DETAIL = Configuration.SERVER_HOST + "/crackdown/detail";
	public static final String URL_CRACKDOWN_DELETE = Configuration.SERVER_HOST + "/crackdown/delete";
	public static final String URL_CRACKDOWN_RANKING = Configuration.SERVER_HOST + "/crackdown/list/rank";
	public static final String URL_REGISTER_CRACKDOWN = Configuration.SERVER_HOST + "/crackdown/register";
	public static final String URL_REGISTER_CRACKDOWN_COMMENT = Configuration.SERVER_HOST + "/crackdown/comment/register";
	public static final String URL_UPDATE_ACTION = Configuration.SERVER_HOST + "/crackdown/action";
	public static final String URL_CHAUFFEUR_COMPANY_LIST = Configuration.SERVER_HOST + "/chauffeur_company/list";
	public static final String URL_CHAUFFEUR_COMPANY_DETAIL = Configuration.SERVER_HOST + "/chauffeur_company/detail";
	public static final String URL_CHAUFFEUR_COMPANY_UPDATE_BOOKMARK = Configuration.SERVER_HOST + "/chauffeur_company/update/bookmark";
	public static final String URL_VIRTUAL_NUMBER_REGISTER = Configuration.SERVER_HOST + "/virtual_number/register";
	public static final String URL_VIRTUAL_NUMBER_UNREGISTER = Configuration.SERVER_HOST + "/virtual_number/unregister";
	public static final String URL_UPLOAD_FILE = Configuration.SERVER_HOST + "/crackdown/upload/file";
	public static final String URL_SHARE_FILE = Configuration.SERVER_HOST + "/crackdown/share/file";
	public static final String URL_NOTICE_LIST = Configuration.SERVER_HOST + "/notice/user/list";
}
